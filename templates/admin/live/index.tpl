{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">频道管理</li>
                <li class=""><a href="{:url('admin/live/add')}">添加频道</a></li>
            </ul>
            <div class="layui-tab-content">

                <form class="layui-form layui-form-pane" action="{:url('admin/live/index')}" method="get">
                    <div class="layui-inline">
                        <label class="layui-form-label">关键词</label>
                        <div class="layui-input-inline">
                            <input type="text" name="keyword" value="{$keyword}" placeholder="请输入关键词" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button class="layui-btn">搜索</button>
                    </div>
                </form>
                <hr>

                <form action="" method="post" class="ajax-form">
                    <div class="layui-tab-item layui-show">
                        <table class="layui-table">
                            <thead>
                            <tr>
                                <th style="width: 15px;"><input type="checkbox" class="check-all"></th>
                                <th style="width: 30px;">ID</th>
                                <th style="width: 30px;">排序</th>
                                <th>频道名称</th>
                                <th style="width: 120px;">状态</th>
                                <th style="width: 200px;">发布时间</th>
                                <th style="width: 120px;">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach name="article_list" item="vo"}
                            <tr>
                                <td><input type="checkbox" name="ids[]" value="{$vo.id}"></td>
                                <td>{$vo.id}</td>
                                <td>{$vo.sort}</td>
                                <td>{$vo.title}</td>
                                <td>{php}echo $vo['status']==1 ? '已审核' : '未审核';{/php}</td>
                                <td>{$vo.create_time | date='Y-m-d H:i:s'}</td>
                                <td style="text-align: center">
                                    <a href="{:url('admin/live/edit',['id'=>$vo['id']])}" class="layui-btn layui-btn-normal layui-btn-sm">编辑</a>
                                    <a href="{:url('admin/live/delete',['id'=>$vo['id']])}" class="layui-btn layui-btn-danger layui-btn-sm ajax-delete">删除</a>
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        <!--分页-->
                        {$article_list|raw}
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{/block}