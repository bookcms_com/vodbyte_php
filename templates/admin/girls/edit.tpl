{extend name="base" /}
{block name="header"}
{/block}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class=""><a href="{:url('admin/girls/index')}">美图管理</a></li>
                <li class="layui-this">编辑美图</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <form class="layui-form form-container" action="{:url('admin/girls/update')}" method="post">
                        <div class="layui-form-item">
                            <label class="layui-form-label">频道美图</label>
                            <div class="layui-input-block">
                                <input type="text" name="title" value="{$girl.title}" required  lay-verify="required" placeholder="请输入标题" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">缩略图</label>
                            <div class="layui-input-block">
                                <input type="text" name="url" value="{$girl.url}" style="width: 80%" placeholder="（选填）请上传封面" class="layui-input layui-input-inline" id="thumb-url">
                                <button type="button" class="layui-btn" data-upload-url="{:url("/upload/upload")}" id="thumb"><i class="layui-icon">&#xe67c;</i>选择文件</button>
                            </div>
                        </div>
                        <div class="layui-form-item layui-form-text">
                            <label class="layui-form-label">美图内容</label>
                            <div class="layui-input-block">
                                <textarea name="content" placeholder="请输入内容" style="min-height: 400px" class="layui-textarea">{$girl.content}</textarea>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">状态</label>
                            <div class="layui-input-block">
                                <input type="radio" name="status" value="1" title="已审核" {if condition="$girl.status==1"} checked="checked"{/if}>
                                <input type="radio" name="status" value="0" title="未审核" {if condition="$girl.status==0"} checked="checked"{/if}>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序</label>
                            <div class="layui-input-block">
                                <input type="text" name="sort" value="{$girl.sort}" required  lay-verify="required" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <input type="hidden" name="id" value="{$girl.id}">
                                <button class="layui-btn" lay-submit lay-filter="*">更新</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}