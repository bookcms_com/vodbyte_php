{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">用户管理</li>
                <li class=""><a href="{:url('admin/user/add')}">添加用户</a></li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">

                    <form class="layui-form layui-form-pane" action="{:url('admin/user/index')}" method="get">
                        <div class="layui-inline">
                            <label class="layui-form-label">关键词</label>
                            <div class="layui-input-inline">
                                <input type="text" name="keyword" value="{$keyword}" placeholder="请输入关键词" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button class="layui-btn">搜索</button>
                        </div>
                    </form>
                    <hr>

                    <table class="layui-table">
                        <thead>
                        <tr>
                            <th style="width: 30px;">ID</th>
                            <th>昵称</th>
                            <th>Email</th>
                            <th>性别</th>
                            <th>会员到期</th>
                            <th>状态</th>
                            <th>最后登录时间</th>
                            <th>最后登录IP</th>
                            <th style="width: 130px;">操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach name="user_list" item="vo"}
                        <tr>
                            <td>{$vo.id}</td>
                            <td>{$vo.nickname}</td>
                            <td>{$vo.email}</td>
                            <td>{php}echo $vo['gender']==1 ? '男' : '女';{/php}</td>
                            <td>{$vo.vip_time | date="Y-m-d H:i:s"}</td>
                            <td>{php}echo $vo['status']==1 ? '启用' : '禁用';{/php}</td>
                            <td>{$vo.last_login_time|date="Y-m-d H:i:s"}</td>
                            <td>{$vo.last_login_ip}</td>
                            <td>
                                <a href="{:url('admin/user/edit',['id'=>$vo['id']])}" class="layui-btn layui-btn-normal layui-btn-sm">编辑</a>
                                <a href="{:url('admin/user/delete',['id'=>$vo['id']])}" class="layui-btn layui-btn-danger layui-btn-sm ajax-delete">删除</a>
                            </td>
                        </tr>
                        {/foreach}
                        </tbody>
                    </table>
                    <!--分页-->
                    {$user_list|raw}
                </div>
            </div>
        </div>
    </div>
</div>
{/block}