{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">视频管理</li>
                <li class=""><a href="{:url('admin/vod/add')}">添加视频</a></li>
            </ul>
            <div class="layui-tab-content">

                <form class="layui-form layui-form-pane" action="{:url('admin/vod/index')}" method="get">
                    <div class="layui-inline">
                        <label class="layui-form-label">分类</label>
                        <div class="layui-input-inline">
                            <select name="cid">
                                <option value="0">全部</option>
                                {foreach name="category_list" item="vo"}
                                <option value="{$vo.id}"> {$vo.name}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">关键词</label>
                        <div class="layui-input-inline">
                            <input type="text" name="keyword" value="{$keyword}" placeholder="请输入关键词" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button class="layui-btn">搜索</button>
                    </div>
                </form>
                <hr>

                <form action="" method="post" class="ajax-form">
                    <button type="button" class="layui-btn layui-btn-small ajax-action" data-action="{:url('admin/vod/toggle',['type'=>'audit'])}">审核</button>
                    <button type="button" class="layui-btn layui-btn-warm layui-btn-small ajax-action" data-action="{:url('admin/vod/toggle',['type'=>'cancel_audit'])}">取消审核</button>
                    <button type="button" class="layui-btn layui-btn-danger layui-btn-small ajax-action" data-action="{:url('admin/vod/delete')}">删除</button>
                    <div class="layui-tab-item layui-show">
                        <table class="layui-table">
                            <thead>
                            <tr>
                                <th style="width: 15px;"><input type="checkbox" class="check-all"></th>
                                <th style="width: 30px;">ID</th>
                                <th style="width: 30px;">排序</th>
                                <th>标题</th>
                                <th>来源</th>
                                <th>分类</th>
                                <th>状态</th>
                                <th>更新时间</th>
                                <th style="width: 120px;">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach name="vod_list" item="vo"}
                            <tr>
                                <td><input type="checkbox" name="ids[]" value="{$vo.id}"></td>
                                <td>{$vo.id}</td>
                                <td>{$vo.sort}</td>
                                <td>{$vo.vod_name}</td>
                                <td>{$vo.vod_play_from}</td>
                                <td>{$category_list[$vo['type_id']]['name']}</td>
                                <td>{php}echo $vo['vod_status']==1 ? '已审核' : '未审核';{/php}</td>
                                <td>{$vo.vod_time | date='Y-m-d H:i:s'}</td>
                                <td style="text-align: center">
                                    <a  lay-text="编辑-{$vo.vod_name}" lay-href="{:url('admin/vod/edit',['id'=>$vo['id']])}" class="layui-btn layui-btn-normal layui-btn-sm">编辑</a>
                                    <a href="{:url('admin/vod/delete',['id'=>$vo['id']])}" class="layui-btn layui-btn-danger layui-btn-sm ajax-delete">删除</a>
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        <!--分页-->
                        {$vod_list|raw}
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{/block}