{extend name="base" /}
{block name="header"}
<script src="__STATIC__/js/wangEditor.min.js"></script>
<script>
    var players_arr_len = 0;
</script>
{/block}
{block name="body"}
<div class="layui-fluid">
        <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class=""><a href="{:url('admin/vod/index')}">视频管理</a></li>
                <li class="layui-this">添加视频</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <form class="layui-form layui-form-pane" action="{:url('admin/vod/save')}" method="post">
                        <div class="layui-form-item">
                            <label class="layui-form-label">参数：</label>
                            <div class="layui-input-inline w150">
                                <select name="type_id" lay-filter="type_id">
                                    <option value="">请选择分类</option>
                                    {volist name="category_list" id="vo"}
                                        <option value="{$vo.id}">{$vo.name}</option>
                                    {/volist}
                                </select>
                            </div>
                            <div class="layui-input-inline w150">
                                <select name="vod_level">
                                    <option value="">选择推荐</option>
                                    <option value="1" >幻灯片</option>
                                    <option value="2" >首页幻灯片</option>
                                </select>
                            </div>
                            <div class="layui-input-inline w150">
                                <select name="vod_levels">
                                    <option value="">选择热播推荐</option>
                                    <option value="1" >热播推荐</option>
                                    <option value="0" >不推荐</option>
                                </select>
                            </div>
                            <div class="layui-input-inline w120">
                                <select name="vod_status">
                                    <option value="1" >已审核</option>
                                    <option value="0" >未审核</option>
                                </select>
                            </div>
                            <div class="layui-input-inline w120">
                                <select name="vod_lock">
                                    <option value="0">未锁</option>
                                    <option value="1" >锁定</option>
                                </select>
                            </div>
                            <div class="layui-input-inline w120">
                                <select name="vod_isend">
                                    <option value="1" >已完结</option>
                                    <option value="0" selected>未完结</option>
                                </select>
                            </div>
                            <div class="layui-input-inline w120">
                                <select name="vod_copyright">
                                    <option value="0" >关闭版权处理</option>
                                    <option value="1" >开启版权处理</option>
                                </select>
                            </div>
                            <div class="layui-input-inline w110">
                                <input type="checkbox" name="uptime" title="更新时间" value="1" checked class="layui-checkbox checkbox-ids" lay-skin="primary">
                            </div>
                        </div>
                        <div class="layui-form-item ">
                            <label class="layui-form-label">标题：</label>
                            <div class="layui-input-inline w500">
                                <input type="text" class="layui-input" value="" placeholder="请输入" name="vod_name" id="vod_name">
                            </div>
                            <label class="layui-form-label">副标：</label>
                            <div class="layui-input-inline ">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_title" id="vod_title">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">别名：</label>
                            <div class="layui-input-inline w500">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_en">
                            </div>
                            <label class="layui-form-label">首字母：</label>
                            <div class="layui-input-inline w70">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_letter">
                            </div>
                            <label class="layui-form-label">简拼：</label>
                            <div class="layui-input-inline w100">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_first_letter">
                            </div>
                            <label class="layui-form-label">高亮：</label>
                            <div class="layui-input-inline w70">
                                <input type="text" class="layui-input color" value="" placeholder="" name="vod_color">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">TAG：</label>
                            <div class="layui-input-inline w500  ">
                                <input type="text" class="layui-input" value="" placeholder=""  name="vod_tag" id="vod_tag">
                            </div>
                            <div class="layui-input-inline w120">
                                <input type="checkbox" name="uptag" title="自动生成" value="1" class="layui-checkbox checkbox-ids" lay-skin="primary">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">备注：</label>
                            <div class="layui-input-inline w500">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_remarks" id="vod_remarks">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">总集数：</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_total" id="vod_total">
                            </div>
                            <label class="layui-form-label">连载数：</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_serial" id="vod_serial">
                            </div>
                            <label class="layui-form-label">上映日期：</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_pubdate" id="vod_pubdate">
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">主演：</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_actor" id="vod_actor">
                            </div>
                            <label class="layui-form-label">导演：</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_director" id="vod_director">
                            </div>
                            <label class="layui-form-label">编剧：</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_writer" id="vod_writer">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">电视频道：</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_tv">
                            </div>
                            <label class="layui-form-label">节目周期：</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_weekday">
                            </div>
                            <label class="layui-form-label">视频时长：</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_duration" id="vod_duration">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">豆瓣评分：</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="" placeholder="豆瓣网评分值" name="vod_douban_score" id="vod_douban_score">
                            </div>
                            <label class="layui-form-label">豆瓣ID：</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" value="" placeholder="" name="vod_douban_id" id="vod_douban_id">
                            </div>
                            <div class="layui-input-inline ">
                                <button type="button" class="layui-btn" id="btn_douban">查询数据</button>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">关联视频：</label>
                            <div class="layui-input-inline w500">
                                <input type="text" class="layui-input" value="" placeholder="如“变形金刚”1、2、3部ID分别为11,12,13或将每部都填“变形金刚”" name="vod_rel_vod">
                            </div>
                            <div class="layui-input-inline ">
                                <a class="layui-btn j-iframe" data-href="/admin.php/admin/vod/data.html?select=1&input=vod_rel_vod" href="javascript:;" title="查询数据">查询数据</a>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">扩展分类：</label>
                            <div class="layui-input-inline w500">
                                <input type="text" class="layui-input" value="" placeholder="" id="vod_class" name="vod_class">
                            </div>
                            <div class="layui-input-inline w500 vod_class_label">
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">上映年代：</label>
                            <div class="layui-input-inline w500">
                                <input type="text" class="layui-input" value="" placeholder="" id="vod_year" name="vod_year">
                            </div>
                            <div class="layui-input-inline w500 vod_year_label">

                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">发行地区：</label>
                            <div class="layui-input-inline w500">
                                <input type="text" class="layui-input" value="" placeholder="" id="vod_area" name="vod_area">
                            </div>
                            <div class="layui-input-inline w500 vod_area_label">

                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">对白语言：</label>
                            <div class="layui-input-inline w500">
                                <input type="text" class="layui-input" value="" placeholder="" id="vod_lang" name="vod_lang">
                            </div>
                            <div class="layui-input-inline w500 vod_lang_label">

                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">影片版本：</label>
                            <div class="layui-input-inline w500">
                                <input type="text" class="layui-input" value="" placeholder="" id="vod_version" name="vod_version">
                            </div>
                            <div class="layui-input-inline w500 vod_version_label">

                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">资源类别：</label>
                            <div class="layui-input-inline w500">
                                <input type="text" class="layui-input" value="" placeholder="" id="vod_state" name="vod_state">
                            </div>
                            <div class="layui-input-inline w500 vod_state_label">

                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">图片：</label>
                            <div class="layui-input-inline w500 upload">
                                <input type="text" class="layui-input upload-input" style="max-width:100%;" value="" placeholder="" id="vod-pic-url" name="vod_pic">
                            </div>
                            <div class="layui-input-inline ">
                                <button type="button" class="layui-btn" data-upload-url="{:url("/upload/upload")}" id="vod-pic"><i class="layui-icon">&#xe67c;</i>上传图片</button>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">缩略图：</label>
                            <div class="layui-input-inline w500 upload">
                                <input type="text" class="layui-input upload-input" style="max-width:100%;" value="" placeholder="" id="thumb-url" name="vod_pic_thumb">
                            </div>
                            <div class="layui-input-inline ">
                                <button type="button" class="layui-btn" data-upload-url="{:url("/upload/upload")}" id="thumb"><i class="layui-icon">&#xe67c;</i>上传图片</button>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">海报图：</label>
                            <div class="layui-input-inline w500 upload">
                                <input type="text" class="layui-input" style="max-width:100%;" value="" placeholder="" id="vod-pic-slide-url" name="vod_pic_slide">
                            </div>
                            <div class="layui-input-inline ">
                                <button type="button" class="layui-btn" data-upload-url="{:url("/upload/upload")}"  id="vod-pic-slide"><i class="layui-icon">&#xe67c;</i>上传图片</button>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">简介：</label>
                            <div class="layui-input-block">
                                <textarea name="vod_blurb" cols="" rows="3" class="layui-textarea"  placeholder="不填写将自动从第一页详情里获取前100个字" style="height:40px;"></textarea>
                            </div>
                        </div>
                        <div id="player_list" class="contents">
                        </div>
                        <div class="layui-form-item">
                            <label class=""><button class="layui-btn radius j-player-add" type="button">添加一组播放</button></label>
                            <div class="layui-input-block">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">跳过开头</label>
                            <div class="layui-input-block">
                                <input type="text" name="vod_skip_time" value="0" required  lay-verify="number" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">播放器排序:</label>
                            <div class="layui-input-block">
                                <input type="radio" name="vod_custom_sort" value="0" title="默认排序" checked>
                                <input type="radio" name="vod_custom_sort" value="1" title="自定义排序" >
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">详细介绍：</label>
                            <div class="layui-input-block">
                                <div id="vod_content"></div>
                                <textarea class="vod_content" name="vod_content" style="display: none"></textarea>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序</label>
                            <div class="layui-input-block">
                                <input type="text" name="sort" value="0" required  lay-verify="required" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item center">
                            <div class="layui-input-block">
                                <button type="submit" class="layui-btn" lay-submit lay-filter="*">保 存</button>
                                <button class="layui-btn layui-btn-warm" type="reset">还 原</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}
{block name="footer"}
<script type="text/javascript">
    // 编辑器
    const editor = new window.wangEditor(document.getElementById('vod_content'))
    editor.create()

    layui.use(['form','upload', 'layer'], function () {
        // 操作对象
        var form = layui.form
            , layer = layui.layer
            , $ = layui.jquery
            , upload = layui.upload;

        // 验证
        form.verify({
            vod_name: function (value) {
                if (value == "") {
                    return "请输入专题名称";
                }
            }
        });

        editor.config.onchange = function (html) {
            $('.vod_content').val(html)
        }

        $(document).on("click", ".extend", function(){
            $id = $(this).attr('data-id');
            if($id == 'vod_class' || $id == 'vod_keywords'){
                $val = $("input[id='"+$id+"']").val();
                if($val!=''){
                    $val = $val+',';
                }
                if($val.indexOf($(this).text())>-1){
                    return;
                }
                $("input[id='"+$id+"']").val($val+$(this).text());
            }else{
                $("input[id='"+$id+"']").val($(this).text());
            }
        });


        form.on('select(type_id)', function(data){
            getExtend(data.value);
        });

        $("#btn_rnd").click(function(){
            $("#vod_hits").val( rndNum(9999) );
            $("#vod_hits_month").val( rndNum(3000) );
            $("#vod_hits_week").val( rndNum(1000) );
            $("#vod_hits_day").val( rndNum(100) );
            $("#vod_up").val( rndNum(1000) );
            $("#vod_down").val( rndNum(1000) );
            $("#vod_score").val( rndNum(10) );
            $("#vod_score_all").val( rndNum(1000) );
            $("#vod_score_num").val( rndNum(100) );
        });

        var is_load=0;
        $('#btn_douban').click(function(){
            var id = $('#vod_douban_id').val();
            var that=$(this);

            if(id == '' || id < 10000){
                alert('请先填写该影片对应的豆瓣的ID');
                return;
            }
            if(is_load==1){
                return;
            }
            is_load=1;
            that.text('读取中...');
            $.ajax({
                type: 'post',
                dataType: "jsonp",
                jsonp: "callback",
                jsonpCallback:"douban",
                timeout: 5000,
                url: '//' + 'api' + '.' + 'mac'+ 'cms' + '.'+ 'com' + '/douban/index/id/' + id,
                error: function(){
                    alert('请求解析服务器失败');
                },
                complete:function(){
                    is_load=0;
                    that.text('查询数据');
                },
                success:function(r){
                    if(r.code>1){
                        alert(r.msg);
                    }
                    else{
                        if(r.data.vod_total){
                            $('#vod_total').val(r.data.vod_total);
                        }
                        if(r.data.vod_serial){
                            $('#vod_continu').val(r.data.vod_serial);
                        }
                        if(r.data.vod_isend){
                            $('#vod_isend').val(r.data.vod_isend);
                        }
                        if(r.data.vod_name){
                            $('#vod_name').val(r.data.vod_name);
                        }
                        if(r.data.vod_title){
                            $('#vod_title').val(r.data.vod_title);
                        }
                        if(r.data.vod_pic){
                            $('#vod_pic').val(r.data.vod_pic);
                        }
                        if(r.data.vod_year){
                            $('#vod_year').val(r.data.vod_year);
                        }
                        if(r.data.vod_language){
                            $('#vod_language').val(r.data.vod_language);
                        }
                        if(r.data.vod_area){
                            $('#vod_area').val(r.data.vod_area);
                        }
                        if(r.data.vod_states){
                            $('#vod_state').val(r.data.vod_states);
                        }
                        if(r.data.vod_type){
                            $('#vod_type').val(r.data.vod_type);
                        }
                        if(r.data.vod_tag){
                            $('#vod_tag').val(r.data.vod_tag);
                        }
                        if(r.data.vod_actor){
                            $('#vod_actor').val(r.data.vod_actor);
                        }
                        if(r.data.vod_director){
                            $('#vod_director').val(r.data.vod_director);
                        }
                        if(r.data.vod_pubdate){
                            $('#vod_pubdate').val(r.data.vod_pubdate);
                        }
                        if(r.data.vod_writer){
                            $('#vod_writer').val(r.data.vod_writer);
                        }
                        if(r.data.vod_score){
                            $('#vod_score').val(r.data.vod_score);
                        }
                        if(r.data.vod_score_num){
                            $('#vod_score_num').val(r.data.vod_score_num);
                        }
                        if(r.data.vod_score_all){
                            $('#vod_score_all').val(r.data.vod_score_all);
                        }
                        if(r.data.vod_douban_score){
                            $('#vod_douban_score').val(r.data.vod_douban_score);
                        }
                        if(r.data.vod_duration){
                            $('#vod_duration').val(r.data.vod_duration);
                        }
                        if(r.data.vod_content){
                         //   ue.setContent(r.data.vod_content);
                        }
                        if(r.data.vod_class){
                            $('#vod_class').val(r.data.vod_class);
                        }
                        if(r.data.vod_reurl) {
                            $('#vod_reurl').val(r.data.vod_reurl);
                        }
                        if(r.data.vod_author) {
                            $('#vod_author').val(r.data.vod_author);
                        }
                    }
                }
            });
        });


        $('.contents').on('click','.j-editor-clear',function(){
            $(this).parent().parent().find('textarea').val('');
        });
        $('.contents').on('click','.j-editor-remove',function(){
            var datai = $(this).parent().parent().attr('data-i');
            $(this).parent().parent().remove();
        });
        $('.contents').on('click','.j-editor-up',function(){
            var current = $(this).parent().parent();
            var current_index = current.index();
            var current_i = current.attr('data-i');
            var prev = current.prev();
            var prev_i = prev.attr('data-i');
            if(current_index>0){
                current.insertBefore(prev);
            }
        });
        $('.contents').on('click','.j-editor-down',function(){
            var current = $(this).parent().parent();
            var current_index = current.index();
            var current_i = current.attr('data-i');
            var next = current.next();
            var next_i = next.attr('data-i');

            if(next.length>0){
                current.insertAfter(next);
            }
        });

        $('.contents').on('click','.j-editor-xz',function(){
            var arr1,s1,s2,urlarr,urlarrcount;
            s1 = $(this).parent().parent().find('textarea').val(); s2="";
            if (s1.length==0){return false;}
                s1 = s1.replaceAll("\r","");
            arr1 = s1.split("\n");
            arr1len = arr1.length;
            for(j=0;j<arr1len;j++){
                if(arr1[j].length>0){
                    urlarr = arr1[j].split('$'); urlarrcount = urlarr.length-1;
                    if(urlarrcount==0){
                        arr1[j]= getPatName(j,arr1len,arr1[j]) + '$' + arr1[j];
                    }
                    s2+=arr1[j]+"\r\n";
                }
            }
            $(this).parent().parent().find('textarea').val(s2.trim()) ;
        });
        $('.contents').on('click','.j-editor-order',function(){
            var arr1,s1,s2,urlarr,urlarrcount;
            s1 = $(this).parent().parent().find('textarea').val(); s2="";
            if (s1.length==0){return false;}
                s1 = s1.replaceAll("\r","");
            arr1=s1.split("\n");
            for(j=arr1.length-1;j>=0;j--){
                if(arr1[j].length>0){
                    s2+=arr1[j]+"\r\n";
                }
            }
            $(this).parent().parent().find('textarea').val(s2.trim()) ;
        });
        $('.contents').on('click','.j-editor-dn',function(){
            var arr1,s1,s2,urlarr,urlarrcount;
            s1 = $(this).parent().parent().find('textarea').val(); s2="";
            if (s1.length==0){return false;}
                s1 = s1.replaceAll("\r","");
            arr1=s1.split("\n");
            for(j=0;j<arr1.length;j++){
                if(arr1[j].length>0){
                    urlarr = arr1[j].split('$'); urlarrcount = urlarr.length-1;
                    if(urlarrcount==0){
                        arr1[j] = arr1[j];
                    }
                    else{
                        arr1[j] = urlarr[1];
                    }
                    s2+=arr1[j]+"\r\n";
                }
            }
            $(this).parent().parent().find('textarea').val(s2.trim()) ;
        });

        $('.j-player-add').on('click',function(){
            players_arr_len++;
            var tpl='<div class="layui-form-item" data-i="'+players_arr_len+'"><label class="layui-form-label">播放'+(players_arr_len)+'：</label> <div class="layui-input-inline w150"> <input type="text" name="vod_play_from[]" class="layui-input" value="" placeholder="播放来源"> </div> <div class="layui-input-inline w150"><input type="text" name="vod_play_note[]" class="layui-input" placeholder="备注信息" ></div><div class="layui-input-inline w400 p10"><a href="javascript:void(0)" class="j-editor-clear">清空</a>&nbsp;<a href="javascript:void(0)" class="j-editor-remove">删除</a>&nbsp;<a href="javascript:void(0)" class="j-editor-up">上移</a>&nbsp;<a href="javascript:void(0)" class="j-editor-down">下移</a>&nbsp;<a href="javascript:void(0)" class="j-editor-xz">校正</a>&nbsp;<a href="javascript:void(0)" class="j-editor-order">倒序</a>&nbsp;<a href="javascript:void(0)" class="j-editor-dn">去前缀</a>&nbsp;</div><div class="p10 m20"></div><div class="layui-input-block"><textarea id="vod_content'+(players_arr_len)+'" name="vod_play_url[]" class="layui-textarea " style="width:99%;height:250px"></textarea></div></div>';
            $("#player_list").append(tpl);

            form.render('select');
        });

        if(players_arr_len == 0) {
            $('.j-player-add').click();
        }
    });

    function getExtend(id){
        $.post("/admin.php/admin/type/extend.html", {id:id}, function(res) {

            if (res.code == 1) {
                $.each(res.data, function(key, value){
                    $('.vod_'+key+"_label").html('');
                    if(value != ''){
                        $.each(value, function(key2, value2){
                            $(".vod_"+key+"_label").append('<a class="layui-btn layui-btn-xs extend" href="javascript:;" data-id="vod_'+key+'">'+value2+'</a>');
                        });
                    }
                });
            }
        });
    }

    function FindNote(s){
        var res="";
        if (s.indexOf("DVD")>0){
            res="DVD";
        }
        else if (s.indexOf("TS")>0 || s.indexOf("TC")>0 || s.indexOf("抢先版")>0) {
            res="抢先版";
        }
        else if (s.indexOf("HD")>0){
            res="HD";
        }
        else if (s.indexOf("BD")>0){
            res="BD";
        }
        else if (s.indexOf("蓝光高清")>0){
            res="蓝光高清";
        }
        else if (s.indexOf("高清")>0){
            res="高清";
        }
        else if (s.indexOf("VCD")>0){
            res="VCD";
        }

        if (s.indexOf("国粤语")>0){
            res +="国粤语";
        }
        else if (s.indexOf("国语")>0){
            res +="国语";
        }
        else if (s.indexOf("粤语")>0){
            res +="粤语";
        }
        else if (s.indexOf("台语")>0){
            res +="台语";
        }
        else if (s.indexOf("英语")>0){
            res +="英语";
        }
        else if (s.indexOf("中文字幕")>0){
            res +="中文字幕";
        }
        return res;
    }

    function getPatName(n,l,s){
        var res="";
        var rc=false;
        if(s.indexOf("qvod:")>-1 || s.indexOf("bdhd:")>-1 || s.indexOf("cool:")>-1){
            var arr = s.split('|');
            if(arr.length>=2){
                res = arr[2].replace(/[^0-9]/ig,"");
                rc=true;

                if(res!=""){
                    if(res.length>3){
                        res += "期";
                    }
                    else if(l==1){
                        res = "全集";
                    }
                    else{
                        res = '第' + res + '集';
                    }

                }
                else{
                    res = FindNote(s);
                    if (s==""){
                        if (l==1){
                            res="全集";
                        }
                        else{
                            rc=false;
                        }
                    }
                }
            }
        }
        if(!rc){
            res = '第' + (n<9 ? '0' : '') + (n+1) + '集';
        }
        return res;
    }


</script>
{/block}