{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-tab layui-tab-brief">
        <ul class="layui-tab-title">
            <li class=""><a href="{:url('admin/category/index')}">分类管理</a></li>
            <li class="layui-this">添加分类</li>
        </ul>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <form class="layui-form form-container" action="{:url('admin/category/save')}" method="post">
                    <div class="layui-form-item">
                        <label class="layui-form-label">分类名称</label>
                        <div class="layui-input-block">
                            <input type="text" name="name" value="" required  lay-verify="required" placeholder="请输入分类名称" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">扩展分类：</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" value="" placeholder="多个用,号连接" name="extend[class]">
                        </div>
                    </div>
                    <div class="layui-form-item vod-list"   >
                        <label class="layui-form-label">扩展地区：</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" value="" placeholder="多个用,号连接" name="extend[area]">
                        </div>
                    </div>
                    <div class="layui-form-item vod-list"  >
                        <label class="layui-form-label">扩展语言：</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" value="" placeholder="多个用,号连接" name="extend[lang]">
                        </div>
                    </div>
                    <div class="layui-form-item vod-list"  >
                        <label class="layui-form-label">扩展年代：</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" value="" placeholder="多个用,号连接" name="extend[year]">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">首页显示</label>
                        <div class="layui-input-block">
                            <input type="radio" name="is_home" value="1" title="显示" >
                            <input type="radio" name="is_home" value="0" title="隐藏" checked>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                            <input type="radio" name="status" value="1" title="显示" checked>
                            <input type="radio" name="status" value="0" title="隐藏" >
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">排序</label>
                        <div class="layui-input-block">
                            <input type="text" name="sort" value="0" required  lay-verify="required" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" lay-submit lay-filter="*">保存</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>
{/block}
