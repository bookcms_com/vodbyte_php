{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-tab layui-tab-brief">
        <ul class="layui-tab-title">
            <li class=""><a href="{:url('admin/category/index')}">分类管理</a></li>
            <li class="layui-this">编辑分类</li>
        </ul>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <form class="layui-form form-container" action="{:url('admin/category/update')}" method="post">
                    <div class="layui-form-item">
                        <label class="layui-form-label">分类名称</label>
                        <div class="layui-input-block">
                            <input type="text" name="name" value="{$category.name}" required  lay-verify="required" placeholder="请输入分类名称" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">扩展分类：</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" value="{$category.extend.class}" placeholder="多个用,号连接" name="extend[class]">
                        </div>
                    </div>
                    <div class="layui-form-item vod-list" {if condition="$category.mid neq '1'"} style="display:none" {/if} >
                        <label class="layui-form-label">扩展地区：</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" value="{$category.extend.area}" placeholder="多个用,号连接" name="extend[area]">
                        </div>
                    </div>
                    <div class="layui-form-item vod-list" {if condition="$category.mid neq '1'"} style="display:none" {/if}>
                        <label class="layui-form-label">扩展语言：</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" value="{$category.extend.lang}" placeholder="多个用,号连接" name="extend[lang]">
                        </div>
                    </div>
                    <div class="layui-form-item vod-list" {if condition="$category.mid neq '1'"} style="display:none" {/if}>
                        <label class="layui-form-label">扩展年代：</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" value="{$category.extend.year}" placeholder="多个用,号连接" name="extend[year]">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">首页显示</label>
                        <div class="layui-input-block">
                            <input type="radio" name="is_home" value="1" title="显示" {if condition="$category.is_home==1"}checked="checked"{/if}>
                            <input type="radio" name="is_home" value="0" title="隐藏" {if condition="$category.is_home==0"}checked="checked"{/if}>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                            <input type="radio" name="status" value="1" title="显示" {if condition="$category.status==1"}checked="checked"{/if}>
                            <input type="radio" name="status" value="0" title="隐藏" {if condition="$category.status==0"}checked="checked"{/if}>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">排序</label>
                        <div class="layui-input-block">
                            <input type="text" name="sort" value="{$category.sort}" required  lay-verify="required" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <input type="hidden" name="id" value="{$category.id}">
                            <button class="layui-btn" lay-submit lay-filter="*">更新</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>
{/block}
