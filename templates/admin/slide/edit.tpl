{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class=""><a href="{:url('admin/slide/index')}">轮播图管理</a></li>
                <li class="layui-this">编辑轮播图</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <form class="layui-form form-container" action="{:url('admin/slide/update')}" method="post">
                        <div class="layui-form-item">
                            <label class="layui-form-label">名称</label>
                            <div class="layui-input-block">
                                <input type="text" name="name" value="{$slide.name}" required lay-verify="required"
                                       placeholder="请输入名称" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">图片</label>
                            <div class="layui-input-block">
                                <input type="text" name="image" value="{$slide.image}" lay-verify="required" style="width: 80%" placeholder="请上传图片" class="layui-input layui-input-inline" id="image-url">
                                <button type="button" class="layui-btn" id="image"><i class="layui-icon">&#xe67c;</i>上传图片</button>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">链接</label>
                            <div class="layui-input-block">
                                <input type="text" name="url" value="{$slide.url}" placeholder="（选填）请输入链接" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">打开方式</label>
                            <div class="layui-input-block">
                                <input type="radio" name="target" value="_self" title="默认" {if condition="$slide.target=='_self'" }checked="checked" {/if}>
                                <input type="radio" name="target" value="_blank" title="新窗口" {if condition="$slide.target=='_blank'" }checked="checked" {/if}>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">状态</label>
                            <div class="layui-input-block">
                                <input type="radio" name="status" value="1" title="显示" {if condition="$slide.status==1" }checked="checked" {/if}>
                                <input type="radio" name="status" value="0" title="隐藏" {if condition="$slide.status==0" }checked="checked" {/if}>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序</label>
                            <div class="layui-input-block">
                                <input type="text" name="sort" value="{$slide.sort}" required lay-verify="required" placeholder="请输入排序" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <input type="hidden" name="id" value="{$slide.id}">
                                <button class="layui-btn" lay-submit lay-filter="*">更新</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}