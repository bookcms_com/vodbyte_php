{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class=""><a href="{:url('admin/slide/index')}">轮播图管理</a></li>
                <li class="layui-this">添加轮播图</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <form class="layui-form form-container" action="{:url('admin/slide/save')}" method="post">
                        <div class="layui-form-item">
                            <label class="layui-form-label">名称</label>
                            <div class="layui-input-block">
                                <input type="text" name="name" value="" required lay-verify="required" placeholder="请输入名称" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">分类图标</label>
                            <div class="layui-input-block">
                                <input type="text" name="image" lay-verify="required" style="width: 80%" placeholder="请上传图片" class="layui-input layui-input-inline" id="image-url">
                                <button type="button" data-upload-url="{:url("/upload/upload")}" class="layui-btn" id="image"><i class="layui-icon">&#xe67c;</i>上传图片</button>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">链接</label>
                            <div class="layui-input-block">
                                <input type="text" name="url" value="" placeholder="（选填）请输入链接" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">打开方式</label>
                            <div class="layui-input-block">
                                <input type="radio" name="target" value="_self" title="默认" checked="checked">
                                <input type="radio" name="target" value="_blank" title="新窗口">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">状态</label>
                            <div class="layui-input-block">
                                <input type="radio" name="status" value="1" title="显示" checked="checked">
                                <input type="radio" name="status" value="0" title="隐藏">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序</label>
                            <div class="layui-input-block">
                                <input type="text" name="sort" value="0" required lay-verify="required" placeholder="请输入排序" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="*">保存</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}