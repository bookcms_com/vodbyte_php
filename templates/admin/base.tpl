<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>字节点播视频管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!--CSS引用-->
    <link rel="stylesheet" href="__STATIC__/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="__STATIC__/admin/style/admin.css" media="all">
    {block name="header"}{/block}

</head>
<body>

<!--主体-->
{block name="body"}{/block}

<script src="__STATIC__/layui/layui.js"></script>
<script>
    layui.config({
        base: "__STATIC__/admin/" //静态资源所在路径
    }).extend({
        index: "lib/index", //主入口模块
    }).use(["index","{$controller|default='default'}"]);
</script>
<script src="__STATIC__/js/admin.js"></script>
{block name="footer"}{/block}
</body>
</html>

