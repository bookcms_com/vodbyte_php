{extend name="base" /}
{block name="header"}
<style>
    .layui-form-select ul {max-height:200px}
    .layui-btn+.layui-btn{
        margin-left: 0;
        margin-top: 5px;
    }
    .layui-form-pane .layui-form-label {
        width: 130px !important;
    }
    .layui-form-pane .layui-input-block {
        margin-left: 130px;
    }
</style>
{/block}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">批量替换</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <form class="layui-form layui-form-pane" action="{:url('admin/database/replace')}" method="post">
                        <div class="layui-tab">
                            <div class="layui-tab-content">
                                <div class="layui-tab-item layui-show">
                                    <div class="layui-form-item row-fields">
                                        <label class="layui-form-label">选择字段：</label>
                                        <div class="layui-input-block fields" >
                                            {volist name="list" id="vo"}
                                            <a class="layui-btn layui-btn-xs w80 sel-field" data-field="{$vo.Field}" href="javascript:void(0)">{$vo.Field}</a>&nbsp;&nbsp;
                                            {/volist}
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">要替换的字段：</label>
                                        <div class="layui-input-block" >
                                            <input type="text" id="field" name="field" placeholder="" lay-verify="field" class="layui-input">
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label">被替换的内容：</label>
                                        <div class="layui-input-block" >
                                            <textarea name="findstr" placeholder="" lay-verify="findstr" class="layui-textarea"></textarea>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">替换为内容：</label>
                                        <div class="layui-input-block" >
                                            <textarea name="tostr" placeholder="" lay-verify="tostr" class="layui-textarea"></textarea>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">替换条件：</label>
                                        <div class="layui-input-block" >
                                            <input type="text" name="where" placeholder="" value="" class="layui-input">
                                        </div>
                                    </div>

                                    <div class="layui-form-item"></div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-form-item center">
                            <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="*">替换</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}
{block name="footer"}
<script type="text/javascript">
    layui.use(['form', 'layer'], function(){
        // 操作对象
        var form = layui.form
                , layer = layui.layer,
                $ = layui.jquery;

        $(".sel-field").on("click",function (){
            $("#field").val($(this).data('field'));
        });

        // 验证
        form.verify({
            field: function (value) {
                if (value == "") {
                    return "请选择字段";
                }
            },
            findstr: function (value) {
                if (value == "") {
                    return "请填写要替换内容";
                }
            },
            tostr: function (value) {
                if (value == "") {
                    return "请填写被替换内容";
                }
            }
        });

    });
</script>
{/block}
