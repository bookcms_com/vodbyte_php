{extend name="base" /}
{block name="header"}
<link rel="stylesheet" href="__STATIC__/admin/style/login.css" media="all">
{/block}
{block name="body"}
<div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">

    <div class="layadmin-user-login-main">
        <div class="layadmin-user-login-box layadmin-user-login-header">
            <h2>字节点播视频管理系统</h2>
        </div>
        <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-username"></label>
                <input type="text" name="username" id="LAY-user-login-username" lay-verify="required" placeholder="用户名" class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label>
                <input type="password" name="password" id="LAY-user-login-password" lay-verify="required" placeholder="密码" class="layui-input">
            </div>
            <div class="layui-form-item">
                <div class="layui-row">
                    <div class="layui-col-xs7">
                        <label class="layadmin-user-login-icon layui-icon layui-icon-vercode" for="LAY-user-login-vercode"></label>
                        <input type="text" name="verify" id="LAY-user-login-vercode" lay-verify="required" placeholder="图形验证码" class="layui-input">
                    </div>
                    <div class="layui-col-xs5">
                        <div style="margin-left: 10px;">
                            <img src="{:captcha_src()}" alt="点击更换" title="点击更换" onclick="this.src='{:captcha_src()}?time='+Math.random()" class="layadmin-user-login-codeimg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item" style="margin-bottom: 20px;">
                <input type="checkbox" name="remember" lay-skin="primary" title="记住密码">
                <a href="{:url('admin/login/forget')}" class="layadmin-user-jump-change layadmin-link" style="margin-top: 7px;">忘记密码？</a>
            </div>
            <div class="layui-form-item">
                <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="LAY-user-login-submit">登 入</button>
            </div>
        </div>
    </div>

    <div class="layui-trans layadmin-user-login-footer">

        <p>2016-{:date('Y')} &copy; <a href="https://www.vodbyte.com" target="_blank">字节点播</a></p>
        <p>
            <span><a href="https://www.vodbyte.com/doc" target="_blank">帮助文档</a></span>
        </p>
    </div>

</div>

{/block}
{block name="footer"}
<script>
    layui.use(['index','form'], function () {
        var $ = layui.$
            , admin = layui.admin
            , form = layui.form;

        //提交
        form.on('submit(LAY-user-login-submit)', function (obj) {
            //请求登入接口
            admin.req({
                url: "{:url('admin/login/login')}"
                , type: 'POST'
                , data: obj.field
                , done: function (res) {
                    if (res.code == 200) {
                        setTimeout(function (){
                            location.href = "{:url('admin/index/index')}";
                        },1500);
                    }
                    layer.msg(res.msg);
                }
            });

        });

    });
</script>
{/block}