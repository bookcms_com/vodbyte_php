{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">更新管理</li>
                <li class=""><a href="{:url('admin/update/add')}">添加更新</a></li>
            </ul>
            <div class="layui-tab-content">

                <form class="layui-form layui-form-pane" action="{:url('admin/update/index')}" method="get">
                    <div class="layui-inline">
                        <label class="layui-form-label">关键词</label>
                        <div class="layui-input-inline">
                            <input type="text" name="keyword" value="{$keyword}" placeholder="请输入关键词" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button class="layui-btn">搜索</button>
                    </div>
                </form>
                <hr>

                <form action="" method="post" class="ajax-form">
                    <div class="layui-tab-item layui-show">
                        <table class="layui-table">
                            <thead>
                            <tr>
                                <th style="width: 15px;"><input type="checkbox" class="check-all"></th>
                                <th style="width: 30px;">ID</th>
                                <th style="width: 30px;">排序</th>
                                <th>版本类型</th>
                                <th>更新标题</th>
                                <th>文件MD5</th>
                                <th>VER_NAME</th>
                                <th>VER_CODE</th>
                                <th style="width: 120px;">状态</th>
                                <th style="width: 200px;">发布时间</th>
                                <th style="width: 120px;">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach name="info_list" item="vo"}
                            <tr>
                                <td><input type="checkbox" name="ids[]" value="{$vo.id}"></td>
                                <td>{$vo.id}</td>
                                <td>{$vo.sort}</td>
                                <td>{php}echo $vo['app_type']== 'tv' ? '电视版' : '手机版';{/php}</td>
                                <td>{$vo.title}</td>
                                <td>{$vo.md5}</td>
                                <td>{$vo.ver_name}</td>
                                <td>{$vo.ver_code}</td>
                                <td>{php}echo $vo['status']==1 ? '已审核' : '未审核';{/php}</td>
                                <td>{$vo.create_time | date='Y-m-d H:i:s'}</td>
                                <td style="text-align: center">
                                    <a href="{:url('admin/update/edit',['id'=>$vo['id']])}" class="layui-btn layui-btn-normal layui-btn-sm">编辑</a>
                                    <a href="{:url('admin/update/delete',['id'=>$vo['id']])}" class="layui-btn layui-btn-danger layui-btn-sm ajax-delete">删除</a>
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        <!--分页-->
                        {$info_list|raw}
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{/block}