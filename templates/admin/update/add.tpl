{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
        <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class=""><a href="{:url('admin/update/index')}">更新管理</a></li>
                <li class="layui-this">添加更新</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <form class="layui-form form-container" action="{:url('admin/update/save')}" method="post">
                        <div class="layui-form-item">
                            <label class="layui-form-label">更新标题</label>
                            <div class="layui-input-block">
                                <input type="text" name="title" value="" required  lay-verify="required" placeholder="请输入标题" class="layui-input title">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">文件地址</label>
                            <div class="layui-input-block">
                                <input type="text" name="url" value="" style="width: 80%" placeholder="（必填）请上传APK文件" class="layui-input layui-input-inline" id="file-url">
                                <button type="button" class="layui-btn" data-upload-url="{:url("/upload/upload")}" id="file"><i class="layui-icon">&#xe67c;</i>选择文件</button>
                            </div>
                        </div>
                        <div class="layui-form-item layui-form-text">
                            <label class="layui-form-label">更新内容</label>
                            <div class="layui-input-block">
                                <textarea name="content" style="min-height: 300px" class="layui-textarea"></textarea>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">类型</label>
                            <div class="layui-input-block">
                                <input type="radio" name="app_type" value="mobile" title="手机版" checked="checked" />
                                <input type="radio" name="app_type" value="tv" title="电视版"  />
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">状态</label>
                            <div class="layui-input-block">
                                <input type="radio" name="status" value="1" title="已审核" checked="checked" />
                                <input type="radio" name="status" value="0" title="未审核"  />
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序</label>
                            <div class="layui-input-block">
                                <input type="text" name="sort" value="0" required  lay-verify="required" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="*">保存</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}