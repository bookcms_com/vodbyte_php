{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">分类管理</li>
                <li class=""><a href="{:url('admin/help_category/add')}">添加分类</a></li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <table class="layui-table">
                        <thead>
                        <tr>
                            <th style="width: 30px;">ID</th>
                            <th style="width: 30px;">排序</th>
                            <th>分类名称</th>
                            <th>首页</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach name="category_list" item="vo"}
                        <tr>
                            <td>{$vo.id}</td>
                            <td>{$vo.sort}</td>
                            <td>{$vo.name}</td>
                            <td>{php}echo $vo['is_home']==1 ? '显示' : '隐藏';{/php}</td>
                            <td width="120px">
                                <a href="{:url('admin/help_category/edit',['id'=>$vo['id']])}" class="layui-btn layui-btn-normal layui-btn-sm">编辑</a>
                                <a href="{:url('admin/help_category/delete',['id'=>$vo['id']])}" class="layui-btn layui-btn-danger layui-btn-sm ajax-delete">删除</a>
                            </td>
                        </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}