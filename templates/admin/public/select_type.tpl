<form class="layui-form m10" method="post" action="{$url}">
    <input type="hidden" name="col" value="{$col}">
    <input type="hidden" name="ids" value="{$ids}">

    <div class="layui-input-inline">
        <select name="val">
            <option value="">选择分类</option>
            {volist name="type_tree" id="vo"}
                {if condition="$vo.mid eq $mid"}
                <option value="{$vo.id}">{$vo.name}</option>
                {/if}
            {/volist}
        </select>
    </div>
    <div class="layui-input-inline">
        <button type="submit" class="layui-btn" lay-submit="" refresh="{$refresh}" lay-filter="formSubmit">保 存</button>
    </div>
</form>

