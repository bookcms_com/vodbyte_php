{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">內容管理</li>
                <li class=""><a href="{:url('admin/help/add')}">添加內容</a></li>
            </ul>
            <div class="layui-tab-content">

                <form class="layui-form layui-form-pane" action="{:url('admin/help/index')}" method="get">
                    <div class="layui-inline">
                        <label class="layui-form-label">关键词</label>
                        <div class="layui-input-inline">
                            <input type="text" name="keyword" value="{$keyword}" placeholder="请输入关键词" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button class="layui-btn">搜索</button>
                        <button type="button" class="layui-btn layui-btn-danger layui-btn-small ajax-action" data-action="{:url('admin/article/delete')}">删除</button>
                    </div>
                </form>
                <hr>

                <form action="" method="post" class="ajax-form">
                    <div class="layui-tab-item layui-show">
                        <table class="layui-table">
                            <thead>
                            <tr>
                                <th style="width: 15px;"><input type="checkbox" class="check-all"></th>
                                <th style="width: 30px;">ID</th>
                                <th style="width: 30px;">排序</th>
                                <th>标题</th>
                                <th>分类</th>
                                <th>状态</th>
                                <th>发布时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach name="$help_list" item="vo"}
                            <tr>
                                <td><input type="checkbox" name="ids[]" value="{$vo.id}"></td>
                                <td>{$vo.id}</td>
                                <td>{$vo.sort}</td>
                                <td>{$vo.title}</td>
                                <td>{$category_list[$vo['cid']]}</td>
                                <td>{php}echo $vo['status']==1 ? '已审核' : '未审核';{/php}</td>
                                <td style="width: 220px">{$vo.create_time}</td>
                                <td style="width: 120px">
                                    <a href="{:url('admin/help/edit',['id'=>$vo['id']])}" class="layui-btn layui-btn-normal layui-btn-sm">编辑</a>
                                    <a href="{:url('admin/help/delete',['id'=>$vo['id']])}" class="layui-btn layui-btn-danger layui-btn-sm ajax-delete">删除</a>
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        <!--分页-->
                        {$help_list|raw}
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{/block}