{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class=""><a href="{:url('admin/collect/index')}">采集管理</a></li>
                <li class="layui-this">编辑接口</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <form class="layui-form layui-form-pane" action="{:url('admin/collect/update')}" method="post">
                        <div class="layui-form-item">
                            <label class="layui-form-label">资源名称：</label>
                            <div class="layui-input-block">
                                <input type="text" class="layui-input" value="{$info.collect_name}" placeholder="" id="collect_name" name="collect_name">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">资源标识：</label>
                            <div class="layui-input-block">
                                <input type="text" class="layui-input" value="{$info.collect_tag}" placeholder="请输入采集标识,也就是vod表vod_play_from字段" name="collect_tag">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">接口地址：</label>
                            <div class="layui-input-block">
                                <input type="text" class="layui-input" value="{$info.collect_url}" placeholder="请输入采集地址" id="collect_url" name="collect_url">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">附加参数：</label>
                            <div class="layui-input-block">
                                <input type="text" class="layui-input" value="{$info.collect_param}" placeholder="" id="collect_param" name="collect_param">
                            </div>
                            <div class="layui-form-mid layui-word-aux" style="margin-left:110px; ">提示信息：一般&开头，例如老版xml格式采集下载地址需加入&ct=1</div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">接口类型：</label>
                            <div class="layui-input-block">
                                <input name="collect_type" class="collect-type" type="radio" value="1" title="xml" {if condition="$info['collect_type'] eq 1"}checked {/if}>
                                <input name="collect_type" class="collect-type" type="radio" value="2" title="json" {if condition="$info['collect_type'] eq 2"}checked {/if}>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">资源类型：</label>
                            <div class="layui-input-block">
                                <input name="collect_mid" lay-filter="collect_mid" type="radio" value="1" title="视频" {if condition="$info['collect_mid'] eq 1"}checked {/if}>
                         <!--       <input name="collect_mid" lay-filter="collect_mid" type="radio" value="2" title="文章" {if condition="$info['collect_mid'] eq 2"}checked {/if}>
                                <input name="collect_mid" lay-filter="collect_mid" type="radio" value="8" title="演员" {if condition="$info['collect_mid'] eq 8"}checked {/if}>
                                <input name="collect_mid" lay-filter="collect_mid" type="radio" value="9" title="角色" {if condition="$info['collect_mid'] eq 9"}checked {/if}>
                                <input name="collect_mid" lay-filter="collect_mid" type="radio" value="11" title="网址" {if condition="$info['collect_mid'] eq 11"}checked {/if}>  -->
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">数据操作：</label>
                            <div class="layui-input-block">
                                <input name="collect_opt" type="radio" value="0" title="新增+更新" {if condition="$info['collect_opt'] eq 0"}checked {/if}>
                                <input name="collect_opt" type="radio" value="1" title="新增" {if condition="$info['collect_opt'] eq 1"}checked {/if}>
                                <input name="collect_opt" type="radio" value="2" title="更新" {if condition="$info['collect_opt'] eq 2"}checked {/if}>
                            </div>
                            <div class="layui-form-mid layui-word-aux" style="">提示信息：如果某个资源作为副资源不想新增数据，可以只勾选更新。</div>
                        </div>
                        <div class="layui-form-item row_filer" {if condition="$info['collect_mid'] neq '1'"} style="display:none;" {/if}>
                            <label class="layui-form-label">地址过滤：</label>
                            <div class="layui-input-block">
                                <input name="collect_filter" type="radio" value="0" title="不过滤" {if condition="$info['collect_filter'] eq 0"}checked {/if}>
                                <input name="collect_filter" type="radio" value="1" title="新增+更新" {if condition="$info['collect_filter'] eq 1"}checked {/if}>
                                <input name="collect_filter" type="radio" value="2" title="新增" {if condition="$info['collect_filter'] eq 2"}checked {/if}>
                                <input name="collect_filter" type="radio" value="3" title="更新" {if condition="$info['collect_filter'] eq 3"}checked {/if}>
                            </div>
                        </div>
                        <div class="layui-form-item row_filer" {if condition="$info['collect_mid'] neq '1'"} style="display:none;" {/if}>
                            <label class="layui-form-label">过滤代码：</label>
                            <div class="layui-input-block">
                                <input type="text" class="layui-input" value="{$info.collect_filter_from}" placeholder="" id="collect_filter_from" name="collect_filter_from">
                            </div>
                            <div class="layui-form-mid layui-word-aux" style="margin-left:110px; ">过滤提示：多组地址的资源开启白名单后只会入库指定代码的地址。</div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">TV显示：</label>
                            <div class="layui-input-block">
                                <input name="collect_tv_filter" type="radio" value="0" title="不显示" {if condition="$info['collect_tv_filter'] eq 0"}checked {/if}>
                                <input name="collect_tv_filter" type="radio" value="1" title="显示" {if condition="$info['collect_tv_filter'] eq 1"}checked {/if}>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">TV二维码</label>
                            <div class="layui-input-block">
                                <input type="text" name="qrcode_url" value="{$info.qrcode_url}" style="width: 80%" placeholder="（选填）请上传二维码" class="layui-input layui-input-inline" id="qrcode-url">
                                <button type="button" class="layui-btn" data-upload-url="{:url("/upload/upload")}" id="qrcode"><i class="layui-icon">&#xe67c;</i>选择文件</button>
                            </div>
                        </div>
                        <div class="layui-form-item layui-form-text">
                            <label class="layui-form-label">解析接口</label>
                            <div class="layui-input-block">
                                <textarea name="jiexi_urls" placeholder="(选填)请填写解析接口一行一个" class="layui-textarea">{$info.jiexi_urls}</textarea>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">P2P加速：</label>
                            <div class="layui-input-block">
                                <input name="is_p2p" type="radio" value="0" title="否" {if condition="$info['is_p2p'] eq 0"}checked {/if}>
                                <input name="is_p2p" type="radio" value="1" title="是" {if condition="$info['is_p2p'] eq 1"}checked {/if}>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序</label>
                            <div class="layui-input-block">
                                <input type="text" name="sort" value="{$info.sort}" required  lay-verify="required" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item center">
                            <div class="layui-input-block">
                                <input type="hidden" name="id" value="{$info.id}">
                                <button class="layui-btn layui-btn-normal" type="button" id="btnTest" >测 试</button>
                                <button type="submit" class="layui-btn" lay-submit lay-filter="*">保 存</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}
{block name="footer"}
    <script type="text/javascript">
        layui.use(['form', 'layer'], function () {
            // 操作对象
            var form = layui.form
                , layer = layui.layer
                , $ = layui.jquery;

            // 验证
            form.verify({
                collect_name: function (value) {
                    if (value == "") {
                        return "请输入资源名称";
                    }
                },
                collect_url: function (value) {
                    if (value == "") {
                        return "请输入接口地址";
                    }
                }
            });

            $('#btnTest').click(function() {
                var that = $(this);
                var data = 'cjurl='+ $('#collect_url').val() + '&cjflag='+ '&ac=list' + "&collect_type=" + $("[name='collect_type']:checked").val();

                $.post("{:url('admin/collect/test')}",data,function(r){
                    if(r.data.code == 1){
                        layer.msg('测试类型成功，接口类型：'+ r.data.msg ,{time:1800});
                        if(r.data.msg == 'json'){
                            $("input[name='collect_type'][value=2]").attr("checked",true);
                        }
                        else{
                            $("input[name='collect_type'][value=1]").attr("checked",true);
                        }
                        form.render('radio');
                    } else{
                        layer.msg(r.data.msg,{time:1800});
                    }
                });

            });

            form.on('radio(collect_mid)',function(data){
                $('.row_filer').hide();
                if(data.value=='1'){
                    $('.row_filer').show();
                }
            });

        });
    </script>
{/block}