<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!--CSS引用-->
    <link rel="stylesheet" href="__STATIC__/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="__STATIC__/admin/style/admin.css" media="all">

    <style>
        .red {color: red}
    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class=""><a href="{:url('admin/collect/index')}">采集管理</a></li>
                <li class="layui-this">绑定资源</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-inline">
                    <div class="mb10">
                        <div class="layui-input-inline w150 m5">
                            <a href="javascript:;" data-id="" class="select_type red">查看全部资源</a>
                        </div>
                        {volist name="type" id="vo"}
                            <div class="layui-input-inline w150 m5">
                                <a href="javascript:;" data-id="{$vo.type_id}" class="select_type">{$vo.type_name}</a>
                                <a id="{$param['cjflag']}_{$vo.type_id}" data-href="{:url('admin/index/select')}?tab=vod&col={$param['cjflag']}_{$vo.type_id}&ids=1&tpl=select_type&refresh=no&url=collect/bind" data-width="320" data-height="450" class="j-select" >
                                    {if condition="$vo.isbind eq 1"}<span class="red">[{$vo.local_type_name}]</span>{else}[绑定]{/if}
                                </a>
                            </div>
                        {/volist}
                    </div>
                </div>
                {php}
                    $p1 = $param;
                    unset($p1['ac']);
                    $p1_str = http_build_query($p1);
                {/php}
                <form class="layui-form layui-form-pane" action="{:url('admin/collect/api')}" method="get">
                    <div class="layui-inline">
                        <label class="layui-form-label">关键词</label>
                        <div class="layui-input-inline">
                            {volist name="param" id="item"}
                                {neq name="$key" value="wd"}
                            <input type="hidden" name="{$key}" value="{$item}" />
                                {/neq}
                            {/volist}
                            <input type="text" id="wd" name="wd" value="{$param['wd']}" placeholder="请输入关键词" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button class="layui-btn">搜索</button>
                    </div>
                </form>
                <hr>
                <div class="layui-tab-item layui-show">
                    <form class="layui-form " method="post" id="pageListForm">
                        <table class="layui-table" lay-size="sm">
                            <thead>
                            <tr>
                                <th style="width: 15px;">
<!--                                    <input type="checkbox" lay-skin="primary" id="check-all">-->
                                </th>
                                <th >名称</th>
                                <th width="60">分类</th>
                                <th width="60">来源</th>
                                <th width="140">时间</th>
                            </tr>
                            </thead>

                            {volist name="list" id="vo"}
                                <tr>
                                    <td><input type="checkbox" name="ids[]" value="{$vo.vod_id}"  lay-skin="primary"></td>
                                    <td>{$vo.vod_name}</td>
                                    <td>{$vo.type_name}</td>
                                    <td>{$vo.vod_play_from}</td>
                                    <td>{$vo.vod_time|mac_day="color"|raw}</td>
                                </tr>
                            {/volist}
                            </tbody>
                        </table>
                        <div class="layui-btn-group">
                            <a href="{:url('admin/collect/api')}?{$p1_str | raw}&ac=cjsel" class="layui-btn layui-btn-primary"><i class="layui-icon">&#xe654;</i>采选中</a>
                            <a href="{:url('admin/collect/api')}?{$p1_str | raw}&h=24&ac=cjday"  class="layui-btn layui-btn-primary"><i class="layui-icon">&#xe654;</i>采当天</a>
                            <a href="{:url('admin/collect/api')}?{$p1_str | raw}&ac=cjall"  class="layui-btn layui-btn-primary"><i class="layui-icon">&#xe654;</i>采全部</a>
                        </div>

                        <div id="pages" class="center"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="__STATIC__/layui/layui.js"></script>
<script type="text/javascript">
    var curUrl = "{:url('admin/collect/api')}?{$param_str | raw}";

    function changeParam(url,name,value)
    {
        var newUrl="";
        var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
        var tmp = name + "=" + value;
        if(url.match(reg) != null) {
            newUrl= url.replace(eval(reg),'&'+tmp + '&');
        }
        else {
            if(url.match("[\?]")) {
                newUrl= url + "&" + tmp;
            }
            else{
                newUrl= url + "?" + tmp;
            }
        }
        return newUrl;
    }

    layui.use(['jquery','laypage', 'layer','form'], function() {
        var laypage = layui.laypage, layer = layui.layer, form = layui.form;
        var $ = layui.jquery;

        laypage.render({
            elem: 'pages'
            ,count: {$total}
            ,limit: {$limit}
            ,curr: {$page}
            ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
            ,jump: function(obj,first){
                if(!first){
                    location.href = curUrl.replace('%7Bpage%7D',obj.curr).replace('%7Blimit%7D',obj.limit);
                }
            }
        });

        $("#check-all").click(function () {
            if ($(this).prop("checked")){
                alert("选中");
            } else {
                alert("没有选中");
            }
        });

        $('#check-all').on('click', function () {
            alert("2222");

            $(this).parents('table').find('input[type="checkbox"]').prop('checked', $(this).prop('checked'));
        });

        $('#wd').on('keydown', function (event) {
            if (event.keyCode == 13) {
                $('.j-btn').click();
                return false;
            }
        });

        $('.j-btn').click(function(){
            var wd = $('input[name="wd"]').val();
            var url = changeParam(curUrl,'wd',wd);
            location.href = url.replace('%7Bpage%7D',1).replace('%7Blimit%7D','');
        });

        $('.select_type').click(function(){
            var t = $(this).attr('data-id');
            var url = changeParam(curUrl,'t',t);
            location.href = url.replace('%7Bpage%7D',1).replace('%7Blimit%7D','');
        });

        /*弹出选择设置*/
        $(".j-select").on('click',function() {
            var that = $(this);
            _url = that.attr('data-href'),
                _title = that.attr('data-title'),
                _width = that.attr('data-width') ? that.attr('data-width')+'' : 750,
                _height = that.attr('data-height') ? that.attr('data-height')+'' : 500,
                _full = that.attr('data-full'),
                _checkbox = that.attr('data-checkbox');

            if (that.parents('form')[0]) {
                var query = that.parents('form').serialize();
            } else {
                var query = $('#pageListForm').serialize();
            }
            if(_checkbox && !query){
                return;
            }
            $.post(_url, query, function(res) {
                layer.closeAll('dialog');
                var lay = layer.open({type:1, title:_title, content:res, area: [_width+'px', _height+'px']});
                form.render('select');
            });
        });

    });

</script>
</body>
</html>


