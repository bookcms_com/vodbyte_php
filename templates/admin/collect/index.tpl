{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">采集管理</li>
                <li class=""><a href="{:url('admin/collect/add')}">添加接口</a></li>
            </ul>
            <div class="layui-tab-content">
                <form action="" method="post" class="ajax-form">
                    <div class="layui-tab-item layui-show">
                        <table class="layui-table">
                            <thead>
                            <tr>
                                <th style="width: 15px;"><input type="checkbox" class="check-all"></th>
                                <th style="width: 30px;">ID</th>
                                <th>排序</th>
                                <th>资源名称</th>
                                <th>资源标识</th>
                                <th>TV显示</th>
                                <th>接口类型</th>
                                <th>资源类型</th>
                                <th>P2P功能</th>
                                <th>资源地址</th>
                                <th style="width: 240px;">采集选项</th>
                                <th style="width: 200px;">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach name="collect_list" item="vo"}
                            <tr>
                                <td><input type="checkbox" name="ids[]" value="{$vo.id}"></td>
                                <td>{$vo.id}</td>
                                <td>{$vo.sort}</td>
                                <td><a href="{:url('/api/timing-collect/' .  $vo.id)}" title="{$vo.collect_name}-定时采集">{$vo.collect_name}</a></td>
                                <td>{$vo.collect_tag}</td>
                                <td>{if condition="$vo['collect_tv_filter'] eq 1"}显示{else/}不显示{/if}</td>
                                <td>{if condition="$vo['collect_type'] eq 1"}xml{else/}json{/if}</td>
                                <td>{if condition="$vo['is_p2p'] eq 1"}开启{else/}关闭{/if}</td>
                                <td>{$vo.collect_mid|mac_get_mid_text}</td>
                                <td><a href="{:url('admin/collect/api')}?{:http_build_query(['ac'=>'list','cjflag'=>md5($vo.collect_url),'cjurl'=>$vo.collect_url,'h'=>'','t'=>'','ids'=>'','wd'=>'','collect_type'=>$vo.collect_type,'mid'=>$vo.collect_mid,'param'=>base64_encode($vo.collect_param),'collect_id'=>$vo.id])}" title="进入资源库">{$vo.collect_url}</a></td>
                                <td>
                                    <a class="layui-btn  layui-btn-sm" href="{:url('api')}?{:http_build_query(['ac'=>'cj','cjflag'=>md5($vo.collect_url),'cjurl'=>$vo.collect_url,'h'=>'24','t'=>'','ids'=>'','wd'=>'','collect_type'=>$vo.collect_type,'mid'=>$vo.collect_mid,'param'=>base64_encode($vo.collect_param),'collect_id'=>$vo.id])}" title="采集当天">采集当天</a>
                                    <a class="layui-btn  layui-btn-normal layui-btn-sm" href="{:url('api')}?{:http_build_query(['ac'=>'cj','cjflag'=>md5($vo.collect_url),'cjurl'=>$vo.collect_url,'h'=>'168','t'=>'','ids'=>'','wd'=>'','collect_type'=>$vo.collect_type,'mid'=>$vo.collect_mid,'param'=>base64_encode($vo.collect_param),'collect_id'=>$vo.id])}" title="采集本周">采集本周</a>
                                    <a class="layui-btn layui-btn-sm" href="{:url('api')}?{:http_build_query(['ac'=>'cj','cjflag'=>md5($vo.collect_url),'cjurl'=>$vo.collect_url,'h'=>'','t'=>'','ids'=>'','wd'=>'','collect_type'=>$vo.collect_type,'mid'=>$vo.collect_mid,'param'=>base64_encode($vo.collect_param),'collect_id'=>$vo.id])}" title="采集所有">采集所有</a>
                                </td>
                                <td style="text-align: center">
                                    <a href="{:url('admin/collect/edit',['id'=>$vo['id']])}" class="layui-btn layui-btn-normal layui-btn-sm">编辑</a>
                                    <a href="{:url('admin/collect/delete',['id'=>$vo['id']])}" class="layui-btn layui-btn-danger layui-btn-sm ajax-delete">删除</a>
                                    <a href="{:url('admin/collect/delete_content',['id'=>$vo['id']])}" class="layui-btn layui-btn-warm layui-btn-sm ajax-delete">清空内容</a>
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        <!--分页-->
                        {$collect_list|raw}
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{/block}