{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-card">
        <!--tab标签-->
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">反馈管理</li>
            </ul>
            <div class="layui-tab-content">

                <form action="" method="post" class="ajax-form">
                    <div class="layui-tab-item layui-show">
                        <button type="button" class="layui-btn layui-btn-danger layui-btn-small ajax-action" data-action="{:url('admin/feedback/delete')}">删除</button>

                        <table class="layui-table">
                            <thead>
                            <tr>
                                <th style="width: 15px;"><input type="checkbox" class="check-all"></th>
                                <th style="width: 30px;">ID</th>
                                <th style="width: 30px;">排序</th>
                                <th>视频ID</th>
                                <th>设备ID</th>
                                <th>内容</th>
                                <th>状态</th>
                                <th>发布时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach name="$info_list" item="vo"}
                            <tr>
                                <td><input type="checkbox" name="ids[]" value="{$vo.id}"></td>
                                <td>{$vo.id}</td>
                                <td>{$vo.sort}</td>
                                <td><a lay-href="{:url('admin/vod/edit',['id'=>$vo['vid']])}">{$vo.vid}</a></td>
                                <td>{$vo.uuid}</td>
                                <td>{$vo.content}</td>
                                <td>{php}echo $vo['status']==1 ? '已审核' : '未审核';{/php}</td>
                                <td style="width: 220px">{$vo.create_time}</td>
                                <td style="width: 60px">
                                    <a href="{:url('admin/feedback/delete',['id'=>$vo['id']])}" class="layui-btn layui-btn-danger layui-btn-sm ajax-delete">删除</a>
                                </td>
                            </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        <!--分页-->
                        {$info_list|raw}
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{/block}