{extend name="base" /}
{block name="body"}
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <form class="layui-form form-container" action="{:url('admin/system/updateSiteConfig')}" method="post">
                    <div class="layui-tab layui-tab-brief">
                        <ul class="layui-tab-title">
                            <li class="layui-this">基本设置</li>
                            <li>采集设置</li>
                            <li>广告设置</li>
                            <li>TV设置</li>
                        </ul>
                        <div class="layui-tab-content">
                            <div class="layui-tab-item layui-show">
                                <div class="layui-form" wid100>
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">网站名称</label>
                                            <div class="layui-input-block">
                                                <input type="text" name="site_config[site_title]"  value="{:get_site_config("site_title")}" class="layui-input">
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">网站域名</label>
                                            <div class="layui-input-block">
                                                <input type="text" name="site_config[site_domain]"  value="{:get_site_config("site_domain")}" class="layui-input">
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">图片域名</label>
                                            <div class="layui-input-block">
                                                <input type="text" name="site_config[pic_domain]" placeholder="可为空"  value="{:get_site_config("pic_domain")}" class="layui-input">
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">分享地址</label>
                                            <div class="layui-input-block">
                                                <input type="text" name="site_config[share_url]"  value="{:get_site_config("share_url")}" class="layui-input">
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">P2P_TOKEN</label>
                                            <div class="layui-input-block">
                                                <input type="text" name="site_config[p2p_token]"  value="{:get_site_config("p2p_token")}" class="layui-input">
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">首页样式:</label>
                                            <div class="layui-input-inline">
                                                <input type="radio" name="site_config[home_style]" value="0" title="区块" {:eq_site_config("home_style","0","checked")}>
                                                <input type="radio" name="site_config[home_style]" value="1" title="瀑布流" {:eq_site_config("home_style","1","checked")}>
                                            </div>
                                            <div class="layui-form-mid layui-word-aux"></div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">接口缓存:</label>
                                            <div class="layui-input-inline">
                                                <input type="radio" name="site_config[api_cache]" value="0" title="关闭" {:eq_site_config("api_cache","0","checked")}>
                                                <input type="radio" name="site_config[api_cache]" value="1" title="开启" {:eq_site_config("api_cache","1","checked")}>
                                            </div>
                                            <div class="layui-form-mid layui-word-aux"></div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">搜索二维码</label>
                                            <div class="layui-input-block">
                                                <input type="text" name="site_config[search_qrcode_url]" value="{:get_site_config("search_qrcode_url")}" style="width: 80%" placeholder="（选填）请上传封面" class="layui-input layui-input-inline" id="qrcode-url">
                                                <button type="button" class="layui-btn" data-upload-url="{:url("/upload/upload")}" id="qrcode"><i class="layui-icon">&#xe67c;</i>选择文件</button>
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">播放失败二维码</label>
                                            <div class="layui-input-block">
                                                <input type="text" name="site_config[error_qrcode_url]" value="{:get_site_config("error_qrcode_url")}" style="width: 80%" placeholder="（选填）请上传封面" class="layui-input layui-input-inline" id="error-qrcode-url">
                                                <button type="button" class="layui-btn" data-upload-url="{:url("/upload/upload")}" id="error-qrcode"><i class="layui-icon">&#xe67c;</i>选择文件</button>
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">播放提示信息</label>
                                            <div class="layui-input-block">
                                                <input type="text" name="site_config[play_note_info]"  value="{:get_site_config("play_note_info")}" class="layui-input">
                                            </div>
                                        </div>
                                        <div class="layui-form-item layui-form-text">
                                            <label class="layui-form-label">热门搜索</label>
                                            <div class="layui-input-block">
                                                <textarea name="site_config[hot_search_word]" placeholder="请输入热门搜索关键字一行一个" class="layui-textarea">{:get_site_config("hot_search_word")}</textarea>
                                            </div>
                                        </div>
                                        <div class="layui-form-item layui-form-text">
                                            <label class="layui-form-label">版权信息</label>
                                            <div class="layui-input-block">
                                                <textarea name="site_config[site_copyright]" placeholder="请输入版权信息" class="layui-textarea">{:get_site_config("site_copyright")}</textarea>
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <div class="layui-input-block">
                                                <button class="layui-btn" lay-submit lay-filter="*">确认保存</button>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="layui-tab-item">
                                <div class="layui-form" wid100>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">下载图片:</label>
                                        <div class="layui-input-inline">
                                            <input type="radio" name="site_config[pic_download]" value="0" title="关闭" {:eq_site_config("pic_download","0","checked")}>
                                            <input type="radio" name="site_config[pic_download]" value="1" title="开启" {:eq_site_config("pic_download","1","checked")}>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux"></div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">扩展分类优化:</label>
                                        <div class="layui-input-inline">
                                            <input type="radio" name="site_config[vod_class_filter]" value="0" title="关闭" {:eq_site_config("vod_class_filter","0","checked")}>
                                            <input type="radio" name="site_config[vod_class_filter]" value="1" title="开启" {:eq_site_config("vod_class_filter","1","checked")}>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">将自动过滤扩分类名称里的[片,剧],例如动作片会变为动作;欧美剧会变成欧美;</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">地址二更规则:</label>
                                        <div class="layui-input-inline">
                                            <input type="radio" name="site_config[vod_urlrole]" value="replace" title="替换" {:eq_site_config("vod_urlrole","replace","checked")}>
                                            <input type="radio" name="site_config[vod_urlrole]" value="merge" title="合并" {:eq_site_config("vod_urlrole","merge","checked")}>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">二次更新地址遇到同类型播放器。替换：只保留新提交的地址。合并：整合原有地址和新地址去重。</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">二次更新规则:</label>
                                        <div class="layui-input-block">
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="a" title="播放地址" {:in_site_config("vod_uprule","a","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="c" title="连载数" {:in_site_config("vod_uprule","c","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="d" title="备注" {:in_site_config("vod_uprule","d","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="e" title="导演" {:in_site_config("vod_uprule","e","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="f" title="演员" {:in_site_config("vod_uprule","f","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="g" title="年代" {:in_site_config("vod_uprule","g","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="h" title="地区" {:in_site_config("vod_uprule","h","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="i" title="语言" {:in_site_config("vod_uprule","i","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="j" title="图片" {:in_site_config("vod_uprule","j","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="k" title="详情" {:in_site_config("vod_uprule","k","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="l" title="TAG" {:in_site_config("vod_uprule","l","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="m" title="副标" {:in_site_config("vod_uprule","m","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="n" title="扩展分类" {:in_site_config("vod_uprule","n","checked")}>
<!--                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="o" title="{:lang('writer')}" {:in_site_config("vod_uprule","o","checked")}>-->
<!--                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="p" title="{:lang('version')}" {:in_site_config("vod_uprule","p","checked")}>-->
<!--                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="q" title="{:lang('state')}" {:in_site_config("vod_uprule","q","checked")}>-->
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="r" title="资源简介" {:in_site_config("vod_uprule","r","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="u" title="总集数" {:in_site_config("vod_uprule","u","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_uprule][]" value="v" title="完结" {:in_site_config("vod_uprule","v","checked")}>
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label">入库重复规则</label>
                                        <div class="layui-input-block">
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_inrule][]" value="a" title="标题" checked disabled>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_inrule][]" value="b" title="类型" {:in_site_config("vod_inrule","b","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_inrule][]" value="c" title="年代" {:in_site_config("vod_inrule","c","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_inrule][]" value="d" title="地区" {:in_site_config("vod_inrule","d","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_inrule][]" value="e" title="语言" {:in_site_config("vod_inrule","e","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_inrule][]" value="f" title="演员" {:in_site_config("vod_inrule","f","checked")}>
                                            <input type="checkbox" lay-skin="primary" name="site_config[vod_inrule][]" value="g" title="导演" {:in_site_config("vod_inrule","g","checked")}>
                                        </div>
                                    </div>
                                    <div class="layui-form-item layui-form-text">
                                        <label class="layui-form-label">采集过滤</label>
                                        <div class="layui-input-block">
                                            <textarea name="site_config[collect_filter]" placeholder="请输入需要过滤的字段逗号分隔" class="layui-textarea">{:get_site_config("collect_filter")}</textarea>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <div class="layui-input-block">
                                            <button class="layui-btn" lay-submit lay-filter="*">确认保存</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-tab-item">
                                <div class="layui-form" wid100>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">开启广告:</label>
                                        <div class="layui-input-inline">
                                            <input type="radio" name="site_config[open_ad]" value="false" title="关闭" {:eq_site_config("open_ad","false","checked")}>
                                            <input type="radio" name="site_config[open_ad]" value="true" title="开启" {:eq_site_config("open_ad","true","checked")}>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux"></div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">APP_ID</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="site_config[ad_appid]"  value="{:get_site_config("ad_appid")}" class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">APP_KEY</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="site_config[ad_appkey]"  value="{:get_site_config("ad_appkey")}" class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">启动页ID</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="site_config[ad_splash_id]"  value="{:get_site_config("ad_splash_id")}" class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">bannerID</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="site_config[ad_banner_id]"  value="{:get_site_config("ad_banner_id")}" class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">信息流ID</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="site_config[ad_flow_id]"  value="{:get_site_config("ad_flow_id")}" class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">插屏ID</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="site_config[ad_half_screen_id]"  value="{:get_site_config("ad_half_screen_id")}" class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">激励广告ID</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="site_config[ad_reward_video_id]"  value="{:get_site_config("ad_reward_video_id")}" class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">激励间隔(秒)</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="site_config[ad_reward_video_time]"  value="{:get_site_config("ad_reward_video_time")}" class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <div class="layui-input-block">
                                            <button class="layui-btn" lay-submit lay-filter="*">确认保存</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-tab-item">
                                <div class="layui-form" wid100>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">开启硬解:</label>
                                        <div class="layui-input-inline">
                                            <input type="radio" name="site_config[open_tv_video_decode]" value="0" title="关闭" {:eq_site_config("open_tv_video_decode","0","checked")}>
                                            <input type="radio" name="site_config[open_tv_video_decode]" value="1" title="开启" {:eq_site_config("open_tv_video_decode","1","checked")}>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux"></div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">片头广告:</label>
                                        <div class="layui-input-inline">
                                            <input type="radio" name="site_config[open_tv_ad]" value="0" title="关闭" {:eq_site_config("open_tv_ad","0","checked")}>
                                            <input type="radio" name="site_config[open_tv_ad]" value="1" title="开启" {:eq_site_config("open_tv_ad","1","checked")}>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux"></div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">片头地址:</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="site_config[tv_ad_url]"  value="{:get_site_config("tv_ad_url")}" class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">跳转地址:</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="site_config[tv_ad_jump_url]"  value="{:get_site_config("tv_ad_jump_url")}" class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <div class="layui-input-block">
                                            <button class="layui-btn" lay-submit lay-filter="*">确认保存</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{/block}