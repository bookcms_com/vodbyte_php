# 字节点播影视APP开源后台 

#### 1.上传网站程序

0.  需要mysql5.6或者5.7,php7.2 安装Redis 和 Redis php扩展
1.  数据库文件为`数据库.sql` 导入数据库文件
2.  上传后设置 public 目录为web访问目录
3.  重命名 `application/` 目录下 `config_init` 为 `config`
4.  在 `config/database.php` 文件里配置好数据库信息
5.  设置伪静态规则为`thinkphp` 后台访问路径 `/admin` 用户名 admin 密码 123456