<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\facade\Route;
// 绑定后台域名
$__bind_domain = \think\facade\Config::pull("bind_domain");
Route::domain($__bind_domain['domain'],'admin');

Route::get('api/v1/config','api/v1/config');
Route::get('api/v1/girls_list','api/v1/girls_list');
Route::get('api/v1/top_vod_data_list','api/v1/top_vod_data_list');
Route::get('api/v1/home_data_list','api/v1/home_data_list');
Route::get('api/v1/vod_item_detail_relevance_list','api/v1/vod_item_detail_relevance_list');
Route::get('api/v1/live_list','api/v1/live_list');
Route::get('api/v1/classify_vod_list','api/v1/classify_vod_list');
Route::get('api/v1/classify_title','api/v1/classify_title');
Route::get('api/v1/vod_item_detail','api/v1/vod_item_detail');
Route::get('api/v1/get_hot_search','api/v1/get_hot_search');
Route::get('api/v1/get_search','api/v1/get_search');
Route::get('api/v1/add_ad_log','api/v1/add_ad_log');
Route::get('p/:vid/:play_from/:index','api/v1/vod_url_detail');

// 检查更新
Route::get('api/update','api/update/index');
Route::post('api/v1/feedback','api/v1/feedback');

# -----------------------tv规则开始-------------------------------
Route::get('api_tv/update','tv/update/index');
Route::get('api_tv/v1/config','tv/v1/config');
Route::get('api_tv/v1/home_data_list','tv/v1/home_data_list');
Route::get('api_tv/v1/vod_item_detail_relevance_list','tv/v1/vod_item_detail_relevance_list');
Route::get('api_tv/v1/live_list','tv/v1/live_list');
Route::get('api_tv/v1/classify_vod_list','tv/v1/classify_vod_list');
Route::get('api_tv/v1/classify_title','tv/v1/classify_title');
Route::get('api_tv/v1/vod_item_detail','tv/v1/vod_item_detail');
Route::get('api_tv/v1/get_hot_search','tv/v1/get_hot_search');
Route::get('api_tv/v1/get_search','tv/v1/get_search');


# -----------------------tv规则结束-------------------------------
//定时采集
Route::get('api/timing-collect/:collect_id','api/timingCollect/index');
// 文件上传
Route::post('upload/upload','admin/upload/upload');
Route::get('clear_cache','api/v1/clear_cache');

return [];

