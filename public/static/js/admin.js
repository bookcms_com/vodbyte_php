/**
 * 后台JS主入口
 */

/**
 * 通用上传
 */
function upload(elem) {
    layui.use(['layer', 'upload', 'jquery'], function () {
        //执行实例
        var layer = layui.layer,
            upload = layui.upload;
        var $ = layui.jquery;

        upload.render({
            elem: elem, //绑定元素
            url: "/api/upload/upload", //上传接口
            type: 'image',
            ext: 'jpg|png|gif|bmp|apk',
            accept: 'file',
            before: function () {
                $(elem + "-url").val("");
            },
            done: function (data) {
                //上传完毕回调
                if (data.error === 0) {
                    $(elem + "-url").val(data.url);
                    if (data.oss === 1) {
                        layer.msg("OSS上传成功");
                    }
                } else {
                    layer.msg(data.message);
                }
            }, error: function () {
                //请求异常回调
                layer.msg("上传出错请稍重试!");
            }
        });
    });
}

layui.use(['form', 'layer', 'element', 'laydate', 'jquery'], function () {

    var layer = layui.layer,
        element = layui.element,
        laydate = layui.laydate,
        form = layui.form;
    var $ = layui.jquery;

    /**
     * AJAX全局设置
     */
    $.ajaxSetup({
        type: "post",
        dataType: "json"
    });

    /**
     * 后台侧边菜单选中状态
     */
    $('.layui-nav-item').find('a').removeClass('layui-this');
    // $('.layui-nav-tree').find('a[href*="' + GV.current_controller + '"]').parent().addClass('layui-this').parents('.layui-nav-item').addClass('layui-nav-itemed');

    /**
     * 通用日期时间选择
     */
    $('.datetime').on('click', function () {
        laydate({
            elem: this,
            istime: true,
            format: 'YYYY-MM-DD hh:mm:ss'
        })
    });

    /**
     * 通用表单提交(AJAX方式)
     */
    form.on('submit(*)', function (data) {
        $.ajax({
            url: data.form.action,
            type: data.form.method,
            data: $(data.form).serialize(),
            success: function (info) {
                if (info.code === 1) {
                    setTimeout(function () {
                        location.href = info.url;
                    }, 1000);
                }
                layer.msg(info.msg);
            }
        });

        return false;
    });

    /**
     * 通用批量处理（审核、取消审核、删除）
     */
    $('.ajax-action').on('click', function () {
        var _action = $(this).data('action');
        layer.open({
            shade: false,
            content: '确定执行此操作？',
            btn: ['确定', '取消'],
            yes: function (index) {
                $.ajax({
                    url: _action,
                    data: $('.ajax-form').serialize(),
                    success: function (info) {
                        if (info.code === 1) {
                            setTimeout(function () {
                                location.href = info.url;
                            }, 1000);
                        }
                        layer.msg(info.msg);
                    }
                });
                layer.close(index);
            }
        });

        return false;
    });

    /**
     * 通用全选
     */
    $('.check-all').on('click', function () {
        $(this).parents('table').find('input[type="checkbox"]').prop('checked', $(this).prop('checked'));
    });

    /**
     * 通用删除
     */
    $('.ajax-delete').on('click', function () {
        var _href = $(this).attr('href');
        layer.open({
            shade: false,
            content: '确定删除？',
            btn: ['确定', '取消'],
            yes: function (index) {
                $.ajax({
                    url: _href,
                    type: "get",
                    success: function (info) {
                        if (info.code === 1) {
                            setTimeout(function () {
                                location.href = info.url;
                            }, 1000);
                        }
                        layer.msg(info.msg);
                    }
                });
                layer.close(index);
            }
        });

        return false;
    });

    /**
     * 清除缓存
     */
    $('.clear-cache').on('click', function () {
        var _url = $(this).attr('href');
        if (_url !== 'undefined') {
            $.ajax({
                url: _url,
                success: function (data) {
                    if (data.code === 1) {
                        setTimeout(function () {
                            location.href = location.pathname;
                        }, 1000);
                    }
                    layer.msg(data.msg);
                }
            });
        }

        return false;
    });


});

/**
 * 判断是否url
 * @param domain
 * @returns {boolean}
 */
function isURL(domain) {
    var name = /[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\.?/;
    if (!(name.test(domain))) {
        return false;
    } else {
        return true;
    }
}
