// 內容管理
layui.define(['uploadImage','form','upload','editor'], function(exports){
  var $ = layui.$
  ,form = layui.form
  ,upload = layui.upload
  ,uploadImage = layui.uploadImage;


  uploadImage("#thumb");
  var elem = "#interface";

  upload.render({
    elem: elem, //绑定元素
    url: $(elem).data("upload-url"), //上传接口
    accept: 'file',
    before: function () {
      $(elem + "-url").val("");
    },
    done: function (info) {

      if (info.code === 0) {
        $(elem + "-url").val(info.data.file);
        $(".title").val(info.data.title);
        $(".ext").val(info.data.ext);
        $(".bid").val(info.data.bid);
        $(".preview").val(info.data.svg);
        $(".content").val(info.data.content);
      } else {
        layer.msg(info.msg);
      }

    }, error: function () {
      layer.msg("上传出错请稍重试!");
    }
  });

  //对外暴露的接口
  exports('admin_article', {});
});