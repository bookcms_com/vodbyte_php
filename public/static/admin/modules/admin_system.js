// 分类
layui.define(['form','uploadImage'], function(exports){
  var $ = layui.$
  ,uploadImage = layui.uploadImage
  ,form = layui.form;

  uploadImage("#qrcode");
  uploadImage("#error-qrcode");

  //对外暴露的接口
  exports('admin_system', {});
});