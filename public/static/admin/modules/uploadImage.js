layui.define(['layer', 'upload'], function(exports) {

	var layer = layui.layer,
		upload = layui.upload,
		$ = layui.jquery;

	var upload2 = function(elem) {

		upload.render({
			elem: elem, //绑定元素
			url: $(elem).data("upload-url"), //上传接口
			type: 'image',
			ext: 'jpg|png|gif|bmp',
			accept: 'file',
			before: function () {
				$(elem + "-url").val("");
			},
			done: function (info) {
				if (info.code === 0) {
					$(elem + "-url").val(info.url);
				} else {
					layer.msg(info.msg);
				}
			}, error: function () {
				layer.msg("上传出错请稍重试!");
			}
		});
	};

	exports("uploadImage",upload2);
});