layui.define(function(exports){
  var $ = layui.$
  ,layer = layui.layer
  ,laytpl = layui.laytpl
  ,setter = layui.setter
  ,view = layui.view
  ,admin = layui.admin
  
  //退出
  admin.events.logout = function(){
    //执行退出接口
    admin.req({
      url: "/login/logout"
      ,type: 'get'
      ,data: {}
      ,done: function(res){
        admin.exit();

        if (res.code == 200) {
            location.href = '/';
        }
      }
    });
  };

  //对外暴露的接口
  exports('common', {});
});