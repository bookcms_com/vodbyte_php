// 分类
layui.define(['form','uploadImage'], function(exports){
  var $ = layui.$
  ,form = layui.form
  ,uploadImage = layui.uploadImage;

  form.render();

  uploadImage("#image");

  //对外暴露的接口
  exports('admin_slide', {});
});
