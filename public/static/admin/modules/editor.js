layui.define(['wangeditor','jquery'], function (exports) {
    // 初始化编辑器
    var wang = layui.wangeditor('editor');
    var $ = layui.$;

    wang.create();

    // 配置 onchange 事件
    wang.onchange = function () {
        $(".editor-textarea").html(this.$txt.html());
    };

    $(function () {
        $(".editor-textarea").html(wang.$txt.html());
    });

    exports('editor',{});
});

