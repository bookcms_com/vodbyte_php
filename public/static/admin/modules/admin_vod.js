// 內容管理
layui.define(['uploadImage','form'], function(exports){
  var $ = layui.$
  ,form = layui.form
  ,uploadImage = layui.uploadImage;

  uploadImage("#thumb");
  uploadImage("#vod-pic");
  uploadImage("#vod-pic-slide");

  //对外暴露的接口
  exports('admin_vod', {});
});