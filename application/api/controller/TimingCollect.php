<?php
namespace app\api\controller;

use app\common\controller\HomeBase;
use think\facade\Cache;
use think\facade\Config;

/**
 * 定时采集理
 * Class Collect
 * @package app\admin\controller
 */
class TimingCollect extends HomeBase
{

    protected function initialize()
    {
        parent::initialize();

    }

    public function index()
    {
        $id = (int)$this->request->param("collect_id",0);
        $collect = model("collect")->where(['id' => $id])->cache()->find();

        $param = [
            'collect_id' => $collect['id'],
            'ac' => 'cjday',
            'h' => '24', // 采集24小时内更新
            'cjflag' => md5($collect['collect_url']),
            'cjurl' => $collect['collect_url'],
            'collect_type' => $collect['collect_type'],
            'collect_mid' => $collect['collect_mid'],
        ];

        return $this->vod($param);
    }


    /**
     * 视频采集
     * @param $param
     * @return mixed|void
     */
    public function vod($param)
    {
        if($param['ac'] != 'list'){
            Cache::set('collect_break_vod', url('collect/api').'?'. http_build_query($param) );
        }

        $cachePageKey = "collect_api_page_" . md5(http_build_query($param)) ;

        $page = (int)Cache::get($cachePageKey);
        if ($page == 0 || empty($page)) {
            $param['page'] = 1;
        }else{
            $param['page'] = $page;
        }

        $result = model('collect')->vod($param);

        if ($result['page']['pagecount'] <= $page) {
            Cache::rm($cachePageKey);
        }else{
            Cache::inc($cachePageKey);
        }

        if($result['code'] > 1){
            return $this->error($result['msg']);
        }

        $data = model('Collect')->vod_data($param,$result,0);
        $data .= "--------------------------------<br/>";
        $data .= " 采集ID:" . $param['collect_id'];
        $data .= " 总页码:" . $result['page']['pagecount'];
        $data .= " 采集页码:" . $param['page'];
        die($data);
    }

}