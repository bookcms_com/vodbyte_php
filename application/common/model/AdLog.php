<?php
namespace app\common\model;

use think\facade\Session;
use think\Model;


class AdLog extends Model
{
    protected $insert = ['create_time'];

    /**
     * 创建时间
     * @return bool|string
     */
    protected function setCreateTimeAttr()
    {
        return time();
    }
}