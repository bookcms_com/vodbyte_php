<?php
namespace app\common\model;

use think\facade\Session;
use think\Model;


class Feedback extends Model
{
    protected $insert = ['create_time'];

    /**
     * 反转义HTML实体标签
     * @param $value
     * @return string
     */
    protected function setContentAttr($value)
    {
        return htmlspecialchars_decode($value);
    }

    /**
     * 创建时间
     * @return bool|string
     */
    protected function setCreateTimeAttr()
    {
        return time();
    }

    protected function getCreateTimeAttr($value)
    {
        return date('Y-m-d H:i:s',$value);
    }
}
