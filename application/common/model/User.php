<?php
namespace app\common\model;

use think\Db;
use think\exception\DbException;
use think\facade\Request;
use think\facade\Session;
use think\Model;

class User extends Model
{
    protected $insert = ['create_time'];

    /**
     * 创建时间
     * @return bool|string
     */
    protected function setCreateTimeAttr()
    {
        return time();
    }

    /**
     * 用户id直接登录
     * @param string $userId
     * @return Model
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public static function userIdLogin($userId = '')
    {
        $user = model('user')->where('id','eq',$userId)->find();

        Session::set('user_info',$user);
        Db::name('user')->update(
            [
                'last_login_time' => time(),
                'last_login_ip'   => Request::ip(),
                'id'              => $user['id']
            ]
        );

        return $user;
    }

    /**
     * 用户登陆
     * @param string $login_type
     * @param string $phone
     * @param string $openid
     * @return array
     * @throws
     */
    public static function userLogin($login_type = '',$phone = '',$openid = '')
    {
        $user = model('user');
        $data = array();

        if ($login_type == 'weixin')
        {
            $data = $user->where('openid','eq',$openid)->find();
        }else if ($login_type == 'phone') {
            $data = $user->where('phone','eq',$phone)->find();
        }

        Session::set('user_info',$data);
        Db::name('user')->update(
            [
                'last_login_time' => time(),
                'last_login_ip'   => Request::ip(),
                'id'              => $data['id']
            ]
        );

        return $data;
    }

    /**
     * 检查用户是否存在
     * @param string $login_type
     * @param string $phone
     * @param string $openid
     * @return bool|float|string
     */
    public static function checkUserIsExist($login_type = '',$phone = '',$openid = '')
    {
        $user = model('user');
        $data = false;
        if ($login_type == 'weixin')
        {
            $data = $user->where('openid','eq',$openid)->count('id');
        }else if ($login_type == 'phone') {
            $data = $user->where('phone','eq',$phone)->count('id');
        }
        return $data;
    }

    /**
     * 创建用户
     * @param string $phone
     * @param string $avatar
     * @param string $nickname
     * @param string $password
     * @param string $gender
     * @param string $login_type
     * @param string $openid
     * @return int|string
     */
    public static function createUser ($phone = '',$avatar = '',$nickname = '',$password = '',$gender = '',$login_type = '',$openid = '')
    {
        $user = model('user');
        $avatar = str_replace("http://","https://",$avatar);
        $save = array(
            'avatar' => $avatar,
            'nickname' => $nickname,
            'login_type' => $login_type,
            'gender' => $gender,
            'phone' => $phone,
            'password' => '',
            'openid' => $openid,
            'create_time' => time(),
            'last_login_time' => time(),
            'last_login_ip' => Request::ip(),
        );
        return $user->insert($save,false,true);
    }

}