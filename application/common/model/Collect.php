<?php
namespace app\common\model;
use org\ImageDown;
use org\Pinyin;
use think\Db;
use think\facade\Cache;
use think\facade\Config;
use think\Model;

class Collect extends Model {

    // 设置数据表（不含前缀）
    protected $name = 'collect';

    // 定义时间戳字段名
    protected $createTime = '';
    protected $updateTime = '';

    // 自动完成
    protected $auto       = [];
    protected $insert     = [];
    protected $update     = [];

    public function vod($param)
    {
        if($param['collect_type'] == '2'){
            return $this->vod_json($param);
        } else if($param['collect_type'] == '1'){
            return $this->vod_xml($param);
        }
    }

    public function vod_xml_replace($url)
    {
        $array_url = array();
        $arr_ji = explode('#',str_replace('||','//',$url));
        foreach($arr_ji as $key=>$value){
            $urlji = explode('$',$value);
            if( count($urlji) > 1 ){
                $array_url[$key] = $urlji[0].'$'.trim($urlji[1]);
            }else{
                $array_url[$key] = trim($urlji[0]);
            }
        }
        return implode('#',$array_url);
    }

    /**
     * xml采集
     * @param $param
     * @return array
     */
    public function vod_xml($param)
    {
        $url_param = [];
        $url_param['ac'] = isset($param['ac']) ? $param['ac'] : '';
        $url_param['t'] = isset($param['t']) ? $param['t'] : '';
        $url_param['pg'] = isset($param['page']) ? $param['page'] : '';
        $url_param['h'] = isset($param['h']) ? $param['h'] : '';
        $url_param['ids'] = isset($param['ids']) ? $param['ids'] : '';
        $url_param['wd'] = isset($param['wd']) ? $param['wd'] : '';
        $param['param'] = isset($param['param']) ? $param['param'] : '';
        $param['rday'] = isset($param['rday']) ? $param['rday'] : '';

        if(empty($param['h']) && !empty($param['rday'])){
            $url_param['h'] = $param['rday'];
        }

        if($param['ac'] != 'list'){
            $url_param['ac'] = 'videolist';
        }

        $url = $param['cjurl'];
        if(strpos($url,'?')===false){
            $url .='?';
        }
        else{
            $url .='&';
        }
        $url .= http_build_query($url_param). base64_decode($param['param']);
        $html = curl_url($url);

        if(empty($html['data'])){
            return ['code'=>1001, 'msg'=>'连接API资源库失败，通常为服务器网络不稳定或禁用了采集'];
        }

        $xml = @simplexml_load_string($html['data']);

        if(empty($xml)){
            $labelRule = '<pic>'."(.*?)".'</pic>';
            $labelRule = mac_buildregx($labelRule,"is");
            preg_match_all($labelRule,$html['data'],$tmparr);
            $ec = false;
            foreach($tmparr[1] as $tt){
                if(strpos($tt,'[CDATA')===false){
                    $ec=true;
                    $ne = '<pic>'.'<![CDATA['.$tt .']]>'.'</pic>';
                    $html['data'] = str_replace('<pic>'.$tt.'</pic>',$ne,$html['data']);
                }
            }
            if($ec) {
                $xml = simplexml_load_string($html['data']);
            }

            if(empty($xml)) {
                return ['code' => 1002, 'msg' => 'XML格式不正确，不支持采集'];
            }
        }

        $array_page = [];
        $array_page['page'] = (string)$xml->list->attributes()->page;
        $array_page['pagecount'] = (string)$xml->list->attributes()->pagecount;
        $array_page['pagesize'] = (string)$xml->list->attributes()->pagesize;
        $array_page['recordcount'] = (string)$xml->list->attributes()->recordcount;
        $array_page['url'] = $url;

        $bind_list = Config::pull('bind');

        $key = 0;
        $array_data = [];
        foreach($xml->list->video as $video){
            $bind_key = $param['cjflag'] .'_'.(string)$video->tid;

            $array_data[$key]['type_id'] = 0;
            if(array_key_exists($bind_key,$bind_list)){
                if($bind_list[$bind_key] > 0){
                    $array_data[$key]['type_id'] = $bind_list[$bind_key];
                }
            }

            $array_data[$key]['vod_id'] = (string)$video->id;
            $array_data[$key]['vod_name'] = (string)$video->name;
            $array_data[$key]['vod_remarks'] = (string)$video->note;
            $array_data[$key]['type_name'] = (string)$video->type;
            $array_data[$key]['vod_pic'] = (string)$video->pic;
            $array_data[$key]['vod_lang'] = (string)$video->lang;
            $array_data[$key]['vod_area'] = (string)$video->area;
            $array_data[$key]['vod_year'] = (string)$video->year;
            $array_data[$key]['vod_serial'] = (string)$video->state;
            $array_data[$key]['vod_actor'] = (string)$video->actor;
            $array_data[$key]['vod_director'] = (string)$video->director;
            $array_data[$key]['vod_content'] = (string)$video->des;
            $array_data[$key]['vod_status'] = 1;
            $array_data[$key]['vod_time'] = (string)$video->last;
            $array_data[$key]['vod_total'] = 0;
            $array_data[$key]['vod_isend'] = 1;
            if($array_data[$key]['vod_serial']){
                $array_data[$key]['vod_isend'] = 0;
            }
            //格式化地址与播放器
            $array_from = [];
            $array_url = [];
            $array_server=[];
            $array_note=[];

            //videolist|list播放列表不同
            if($count = @count($video->dl->dd)){
                for($i=0; $i<$count; $i++){
                    $array_from[$i] = (string)$video->dl->dd[$i]['flag'];
                    $array_url[$i] = $this->vod_xml_replace((string)$video->dl->dd[$i]);
                    $array_server[$i] = 'no';
                    $array_note[$i] = '';
                }
            }else{
                $array_from[]=(string)$video->dt;
                $array_url[] ='';
                $array_server[]='';
                $array_note[]='';
            }

            if(strpos(base64_decode($param['param']),'ct=1')!==false){
                $array_data[$key]['vod_down_from'] = implode('$$$', $array_from);
                $array_data[$key]['vod_down_note'] = implode('$$$', $array_note);
            } else{
                $array_data[$key]['vod_play_from'] = implode('$$$', $array_from);
                $array_data[$key]['vod_play_url'] = implode('$$$', $array_url);
                $array_data[$key]['vod_play_note'] = implode('$$$', $array_note);
            }
            $key++;
        }

        $array_type = [];
        $key = 0;
        //分类列表
        if($param['ac'] == 'list'){
            foreach($xml->class->ty as $ty){
                $array_type[$key]['type_id'] = (string)$ty->attributes()->id;
                $array_type[$key]['type_name'] = (string)$ty;
                $key++;
            }
        }

        $res = [
            'code' => 1,
            'msg' => 'xml',
            'page' => $array_page,
            'type' => $array_type,
            'data' => $array_data
        ];
        return $res;
    }

    /**
     * json解析
     * @param $param
     * @return array
     */
    public function vod_json($param)
    {
        $url_param = [];
        $url_param['pg']  = isset($param['page']) ? $param['page'] : '';
        $url_param['ids'] = isset($param['ids']) ? $param['ids'] : "";
        $url_param['h']   = isset($param['h']) ? $param['h'] : "";
        $url_param['t']   = isset($param['t']) ? $param['t'] : "";
        $url_param['ac']  = isset($param['ac']) ? $param['ac'] : "";
        $url_param['wd']  = isset($param['wd']) ? $param['wd'] : "";
        $param['param']  = isset($param['param']) ? $param['param'] : "";

        if($param['ac'] != 'list'){
            $url_param['ac'] = 'videolist';
        }

        $url = $param['cjurl'];
        if(strpos($url,'?')===false){
            $url .='?';
        }
        else{
            $url .='&';
        }
        $url .= http_build_query($url_param). base64_decode($param['param']);
        $html = curl_url($url);

        if(empty($html['data'])){
            return ['code'=>1001, 'msg'=>'连接API资源库失败，通常为服务器网络不稳定或禁用了采集'];
        }

        $json = json_decode($html['data'],true);
        if(!$json){
            return ['code'=>1002, 'msg'=>'JSON格式不正确，不支持采集'];
        }

        $array_page = [];
        $array_page['page'] = $json['page'];
        $array_page['pagecount'] = $json['pagecount'];
        $array_page['pagesize'] = $json['limit'];
        $array_page['recordcount'] = $json['total'];
        $array_page['url'] = $url;

        $bind_list = Config::pull('bind');
        $key = 0;
        $array_data = [];

        foreach($json['list'] as $key => $v){
            $array_data[$key] = $v;
            $bind_key = $param['cjflag'] .'_'.$v['type_id'];

            $array_data[$key]['type_id'] = 0;
            if (array_key_exists($bind_key,$bind_list)) {
                if($bind_list[$bind_key] > 0){
                    $array_data[$key]['type_id'] = $bind_list[$bind_key];
                }
            }

            if(!empty($v['dl'])) {
                //格式化地址与播放器
                $array_from = [];
                $array_url = [];
                $array_server = [];
                $array_note = [];
                //videolist|list播放列表不同
                foreach ($v['dl'] as $k2 => $v2) {
                    $array_from[] = $k2;
                    $array_url[] = $v2;
                    $array_server[] = 'no';
                    $array_note[] = '';
                }

                $array_data[$key]['vod_play_from'] = implode('$$$', $array_from);
                $array_data[$key]['vod_play_url'] = implode('$$$', $array_url);
//                $array_data[$key]['vod_play_server'] = implode('$$$', $array_server);
                $array_data[$key]['vod_play_note'] = implode('$$$', $array_note);
            }
        }

        $array_type = [];
        $key = 0;
        //分类列表
        if($param['ac'] == 'list'){
            foreach($json['class'] as $k => $v){
                $array_type[$key]['type_id'] = $v['type_id'];
                $array_type[$key]['type_name'] = $v['type_name'];
                $key++;
            }
        }

        $res = ['code'=>1, 'msg'=>'json', 'page'=>$array_page, 'type'=>$array_type, 'data'=>$array_data ];
        return $res;
    }

    /**
     * 下载图片
     * @param $pic_download
     * @param $pic_url
     * @param string $flag
     * @return array
     */
    public function syncImages($pic_download,$pic_url,$flag='vod')
    {
        $des = "";
        if($pic_download == 1){
            $img_url = ImageDown::down_load($pic_url, $flag);
            if ($img_url == $pic_url) {
                $des = '<a href="' . $img_url . '" target="_blank">' . $img_url . '</a><font color=red>下载失败!</font>';
            } else {
                $pic_url = $img_url;
                $des = '<a href="' . $img_url . '" target="_blank">' . $img_url . '</a><font color=green>下载成功!</font>';
            }
        }

        return ['pic' => $pic_url,'msg' => $des];
    }

    /**
     * 采集数据
     * @param $param
     * @param $data
     * @param int $show
     * @return array
     */
    public function vod_data($param,$data,$show = 1)
    {
        if($show == 1) {
            mac_echo('当前采集任务vod_data <strong class="green">' . $data['page']['page'] . '</strong>/<span class="green">' . $data['page']['pagecount'] . '</span>页 采集地址&nbsp;' . $data['page']['url'] . '');
        }

        $config = Config::pull('site_config');
        $type_list = model('category')->getTypeList();
        $filter_arr = explode(',',$config['collect_filter']);
        $api_ret_str = '';

        foreach($data['data'] as $k => $v){

            // 删除字段
            ZHTUnset($v,"vod_pic_screenshot,vod_plot_detail,vod_plot_name,vod_plot,vod_down_url,vod_down_note,vod_down_server,vod_down_from,vod_play_server,vod_pwd_down_url,vod_pwd_down,vod_pwd_play_url,vod_pwd_play,vod_pwd_url,vod_pwd,vod_points_down,vod_points_play,vod_points,group_id,vod_jumpurl,vod_tpl_play,vod_tpl,vod_tpl_down");

            $color='red';
            $des='';
            $msg='';
            $tmp='';

            if($v['type_id'] == 0){
                $des = '分类未绑定，跳过err';
            } else if(empty($v['vod_name'])) {
                $des = '数据不完整，跳过err';
            } else if( mac_array_filter($filter_arr,$v['vod_name']) !==false) {
                $des = '数据在过滤单中，跳过err';
            } else {
                unset($v['vod_id']);
                foreach($v as $k2=>$v2){
                    if(strpos($k2,'_content')===false) {
                        $v[$k2] = strip_tags($v2);
                    }
                }

                $v['type_id_1'] = intval($type_list[$v['type_id']]['pid']);
                $v['vod_en'] = Pinyin::get($v['vod_name']);
                $v['vod_letter'] = strtoupper(substr($v['vod_en'],0,1));
                $v['vod_first_letter'] = Pinyin::get($v['vod_name'],"first");
                $v['vod_time_add'] = time();
                $v['vod_time'] = time();
                $v['vod_status'] = 1;

                $v['vod_lock'] = isset($v['vod_lock']) ? intval($v['vod_lock']) : 0;
                if(!empty($v['vod_status'])) {
                    $v['vod_status'] = intval($v['vod_status']);
                }

                $v['vod_year'] = intval($v['vod_year']);
                $v['vod_level'] = isset($v['vod_level']) ? intval($v['vod_level']) : 0;
                $v['vod_hits'] = isset($v['vod_hits']) ? intval($v['vod_hits']) : 0;
                $v['vod_hits_day'] = isset($v['vod_hits_day']) ? intval($v['vod_hits_day']) : 0;
                $v['vod_hits_week'] = isset($v['vod_hits_week']) ? intval($v['vod_hits_week']) : 0;
                $v['vod_hits_month'] = isset($v['vod_hits_month']) ? intval($v['vod_hits_month']) : 0;
                $v['vod_up'] = isset($v['vod_up']) ? intval($v['vod_up']) : 0;
                $v['vod_down'] = isset($v['vod_down']) ? intval($v['vod_down']) : 0;
                $v['vod_score'] = isset($v['vod_score']) ? intval($v['vod_score']) : 0;
                $v['vod_score_all'] = isset($v['vod_score_all']) ? intval($v['vod_score_all']) : 0;
                $v['vod_score_num'] = isset($v['vod_score_num']) ? intval($v['vod_score_num']) : 0;

                $v['vod_total'] = intval($v['vod_total']);
                $v['vod_serial'] = intval($v['vod_serial']);
                $v['vod_isend'] = intval($v['vod_isend']);

                $v['vod_class'] = mac_txt_merge((isset($v['vod_class']) ? intval($v['vod_class']) : ""),$v['type_name']);
                $v['vod_tag'] = mac_format_text((isset($v['vod_tag']) ? intval($v['vod_tag']) : ""));

                $v['vod_actor'] = mac_format_text($v['vod_actor']);
                $v['vod_director'] = mac_format_text($v['vod_director']);
                $v['vod_class'] = mac_format_text($v['vod_class']);
                $v['vod_tag'] = mac_format_text($v['vod_tag']);

                if(empty($v['vod_isend']) && !empty($v['vod_serial'])){
                    $v['vod_isend'] = 0;
                }

                if(empty($v['vod_blurb'])){
                    $v['vod_blurb'] = mac_substring( strip_tags($v['vod_content']) ,100);
                }

                $where = [];
                $where['vod_name'] = $v['vod_name'];
                $blend = false;

                if (strpos($config['vod_inrule'], 'b') !== false) {
                    $where['type_id'] = $v['type_id'];
                }
                if (strpos($config['vod_inrule'], 'c')!==false) {
                    $where['vod_year'] = $v['vod_year'];
                }

                if (strpos($config['vod_inrule'], 'd')!==false) {
                    $where['vod_area'] = $v['vod_area'];
                }
                if (strpos($config['vod_inrule'], 'e')!==false) {
                    $where['vod_lang'] = $v['vod_lang'];
                }
                if (strpos($config['vod_inrule'], 'f')!==false) {
                    $where['vod_actor'] = ['like', mac_like_arr($v['vod_actor']), 'OR'];
                }
                if (strpos($config['vod_inrule'], 'g')!==false) {
                    $where['vod_director'] = $v['vod_director'];
                }

//                if ($config['tag'] == 1) {
//                    $v['vod_tag'] = mac_get_tag($v['vod_name'], $v['vod_content']);
//                }

                if(!empty($where['vod_actor']) && !empty($where['vod_director'])){
                    $blend = true;
                    $GLOBALS['blend'] = [
                        'vod_actor' => $where['vod_actor'],
                        'vod_director' => $where['vod_director']
                    ];
                    unset($where['vod_actor'],$where['vod_director']);
                }

                if(empty($v['vod_play_url'])){
                    $v['vod_play_url'] = '';
                }

                //验证地址
                $cj_play_from_arr = explode('$$$',$v['vod_play_from'] );
                $cj_play_url_arr = explode('$$$',$v['vod_play_url']);
                $cj_play_note_arr = explode('$$$',$v['vod_play_note']);

                $collect_id = $param['collect_id'];
                $collect_filter_from = Db::name('collect')->where('id',$collect_id)->value('collect_filter_from');
                $collect_filter_from_arr = explode(',',$collect_filter_from);

                foreach($cj_play_from_arr as $kk => $vv){
                    if(empty($vv)){
                        unset($cj_play_from_arr[$kk]);
                        continue;
                    }

                    if(in_array($vv,$collect_filter_from_arr)){
                        unset($cj_play_from_arr[$kk]);
                        unset($cj_play_url_arr[$kk]);
                        continue;
                    }

                    $cj_play_url_arr[$kk] = rtrim($cj_play_url_arr[$kk],'#');
                    $cj_play_note_arr[$kk] = $cj_play_note_arr[$kk];
                }
                if (count($cj_play_from_arr) == 0){
                    continue;
                }

                $v['vod_play_from'] = join('$$$',$cj_play_from_arr);
                $v['vod_play_url'] = join('$$$',$cj_play_url_arr);
                $v['vod_play_note'] = join('$$$',$cj_play_note_arr);

                if(empty($v['vod_play_from'])) $v['vod_play_from']='';
                if(empty($v['vod_play_url'])) $v['vod_play_url']='';
                if(empty($v['vod_play_note'])) $v['vod_play_note']='';

                if($blend === false){
                    $info = model('vod')->where($where)->find();
                } else{
                    $info = model('vod')->where($where)
                        ->where(function($query){
                            $query->where('vod_director',$GLOBALS['blend']['vod_director'])->whereOr('vod_actor',$GLOBALS['blend']['vod_actor']);
                        })->find();
                }

                if (!$info) {

                    $tmp = $this->syncImages($config['pic_download'],$v['vod_pic'],'vod');
                    $v['vod_pic'] = (string)$tmp['pic'];
                    $msg = $tmp['msg'];
                    unset($v['type_name']);

                    $res = model('vod')->insert($v);
                    if ($res) {
                        $color = 'green';
                        $des = '新加入库，成功ok。';
                    }

                } else {
                    // 二次更新判断开始
                    if(empty($config['vod_uprule'])){
                        $des = '没有设置任何二次更新项目，跳过。';
                    } else if ($info['vod_lock'] == 1) {
                        $des = '数据已经锁定，跳过。';
                    } else {
                        unset($v['vod_time_add']);

                        $update = [];
                        $ec = false;

                        if (strpos(',' . $config['vod_uprule'], 'a') !== false && !empty($v['vod_play_from'])) {
                            $old_play_from = $info['vod_play_from'];
                            $old_play_url = $info['vod_play_url'];
                            $old_play_note = $info['vod_play_note'];
                            foreach ($cj_play_from_arr as $k2 => $v2) {
                                $cj_play_from = $v2;
                                $cj_play_url = $cj_play_url_arr[$k2];
                                $cj_play_note = $cj_play_note_arr[$k2];

                                if ($cj_play_url == $info['vod_play_url']) {
                                    $des .= '播放地址相同，跳过。';
                                } else if(empty($cj_play_from)) {
                                    $des .= '播放器类型为空，跳过。';
                                } else if(strpos("," . $info['vod_play_from'], $cj_play_from) <= 0) {
                                    $color = 'green';
                                    $des .= '播放组(' . $cj_play_from . ')，新增ok。';
                                    if(!empty($old_play_from)){
                                        $old_play_url .="$$$";
                                        $old_play_from .= "$$$" ;
                                        $old_play_note .= "$$$" ;
                                    }
                                    $old_play_url .= "" . $cj_play_url;
                                    $old_play_from .= "" . $cj_play_from;
                                    $old_play_note .= "" . $cj_play_note;
                                    $ec=true;
                                }  else if (!empty($cj_play_url)) {
                                    $arr1 = explode("$$$", $old_play_url);
                                    $arr2 = explode("$$$", $old_play_from);
                                    $play_key = array_search($cj_play_from, $arr2);
                                    if ($arr1[$play_key] == $cj_play_url) {
                                        $des .= '播放组(' . $cj_play_from . ')，无需更新。';
                                    } else {
                                        $color = 'green';
                                        $des .= '播放组(' . $cj_play_from . ')，更新ok。';
                                        // 二次更新合并
                                        if ($config['vod_urlrole'] == "merge") {
                                            $tmp1 = explode('#',$arr1[$play_key]);
                                            $tmp2 = explode('#',$cj_play_url);
                                            $tmp1 = array_merge($tmp1,$tmp2);
                                            $tmp1 = array_unique($tmp1);
                                            $cj_play_url = join('#',$tmp1);
                                            unset($tmp1,$tmp2);
                                        }
                                        $arr1[$play_key] = $cj_play_url;
                                        $ec=true;
                                    }
                                    $old_play_url = join('$$$', $arr1);
                                }
                            }
                            if($ec) {
                                $update['vod_play_from'] = $old_play_from;
                                $update['vod_play_url'] = $old_play_url;
                                $update['vod_play_note'] = $old_play_note;
                            }
                        }

                        $ec = false;

                        if (strpos(',' . $config['vod_uprule'], 'c')!==false && !empty($v['vod_serial']) && $v['vod_serial']!=$info['vod_serial']) {
                            $update['vod_serial'] = $v['vod_serial'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'd')!==false && !empty($v['vod_remarks']) && $v['vod_remarks']!=$info['vod_remarks']) {

                            $new_remarks = $v['vod_remarks'];
                            $old_remarks = $info['vod_remarks'];
                            $pattern = "/更新至(\d+)集/";
                            preg_match_all($pattern, $new_remarks, $match);
                            preg_match_all($pattern, $old_remarks, $match2);

                            if(count($match[1])>0 and count($match2[1])>0){
                                if($match[1][0]>$match2[1][0]){
                                    $update['vod_remarks'] = $v['vod_remarks'];
                                }
                            }else{
                                $update['vod_remarks'] = $v['vod_remarks'];
                            }
                        }
                        if (strpos(',' . $config['vod_uprule'], 'e')!==false && !empty($v['vod_director']) && $v['vod_director']!=$info['vod_director']) {
                            $update['vod_director'] = $v['vod_director'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'f')!==false && !empty($v['vod_actor']) && $v['vod_actor']!=$info['vod_actor']) {
                            $update['vod_actor'] = $v['vod_actor'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'g')!==false && !empty($v['vod_year']) && $v['vod_year']!=$info['vod_year']) {
                            $update['vod_year'] = $v['vod_year'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'h')!==false && !empty($v['vod_area']) && $v['vod_area']!=$info['vod_area']) {
                            $update['vod_area'] = $v['vod_area'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'i')!==false && !empty($v['vod_lang']) && $v['vod_lang']!=$info['vod_lang']) {
                            $update['vod_lang'] = $v['vod_lang'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'j')!==false && (substr($info["vod_pic"], 0, 4) == "http" || empty($info['vod_pic']) ) && $v['vod_pic'] != $info['vod_pic'] ) {
                            $tmp = $this->syncImages($config['pic_download'],$v['vod_pic'],'vod');
                            $update['vod_pic'] = (string)$tmp['pic'];
                            $msg =$tmp['msg'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'k')!==false && !empty($v['vod_content']) && $v['vod_content']!=$info['vod_content']) {
                            $update['vod_content'] = $v['vod_content'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'l')!==false && !empty($v['vod_tag']) && $v['vod_tag']!=$info['vod_tag']) {
                            $update['vod_tag'] = $v['vod_tag'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'm')!==false && !empty($v['vod_sub']) && $v['vod_sub']!=$info['vod_sub']) {
                            $update['vod_sub'] = $v['vod_sub'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'n')!==false && !empty($v['vod_class']) && $v['vod_class']!=$info['vod_class']) {
                            $update['vod_class'] = mac_txt_merge($info['vod_class'], $v['vod_class']);
                        }
                        if (strpos(',' . $config['vod_uprule'], 'o')!==false && !empty($v['vod_writer']) && $v['vod_writer']!=$info['vod_writer']) {
                            $update['vod_writer'] = $v['vod_writer'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'p')!==false && !empty($v['vod_version']) && $v['vod_version']!=$info['vod_version']) {
                            $update['vod_version'] = $v['vod_version'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'q')!==false && !empty($v['vod_state']) && $v['vod_state']!=$info['vod_state']) {
                            $update['vod_state'] = $v['vod_state'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'r')!==false && !empty($v['vod_blurb']) && $v['vod_blurb']!=$info['vod_blurb']) {
                            $update['vod_blurb'] = $v['vod_blurb'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'u')!==false && !empty($v['vod_total']) && $v['vod_total']!=$info['vod_total']) {
                            $update['vod_total'] = $v['vod_total'];
                        }
                        if (strpos(',' . $config['vod_uprule'], 'v')!==false && !empty($v['vod_isend']) && $v['vod_isend']!=$info['vod_isend']) {
                            $update['vod_isend'] = $v['vod_isend'];
                        }

                        if (empty($info['vod_first_letter']))
                        {
                            $update['vod_first_letter'] = Pinyin::get($v['vod_name'],"first");
                            $des .= "标题首拼更新成功.";
                        }

                        if(count($update) > 0 ){
                            $update['vod_time'] = time();
                            $res = model('vod')->allowField(true)->update($update,['id' => $info['id']]);
                            if ($res) {
                                $color = 'green';
                            }
                        } else{
                            $des = '无需更新。';
                        }

                    }
                }
            }

            if($show == 1) {
                mac_echo( ($k + 1) .'、'. $v['vod_name'] . " <font color=$color>" .$des .'</font>'. $msg.'' );
            } else{
                $api_ret_str .= ($k + 1) .'、'. $v['vod_name'] . " <font color=$color>" .$des .'</font>'. $msg.'' . "\r\n<br/>";
            }

        }

        if($show == 1) {
            if ($param['ac'] == 'cjsel') {
                Cache::rm('collect_break_vod');
                mac_echo("数据采集完成");
                unset($param['ids']);
                $param['ac'] = 'list';
                $url = url('api') . '?' . http_build_query($param);
                $ref = $_SERVER["HTTP_REFERER"];
                if(!empty($ref)){
                   $url = $ref;
                }

                mac_jump($url, 3);
            } else {
                if ($data['page']['page'] >= $data['page']['pagecount']) {
                    Cache::rm('collect_break_vod');
                    mac_echo("数据采集完成");
                    unset($param['page'],$param['ids']);
                    $param['ac'] = 'list';
                    $url = url('api') . '?' . http_build_query($param);
                    mac_jump($url, 3);
                } else {
                    $param['page'] = intval($data['page']['page']) + 1;
                    $url = url('api') . '?' . http_build_query($param);
                    mac_jump($url, 3);
                }
            }
        }else{
            return  $api_ret_str;
        }
    }

}