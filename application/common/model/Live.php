<?php
namespace app\common\model;

use think\facade\Session;
use think\Model;


class Live extends Model
{
    protected $insert = ['create_time'];

    protected function getContentAttr($value)
    {
        $array = explode("\r",$value);
        $data = [];
        if (is_array($array)) {
            foreach ($array as $item) {
                $arr2 = explode(",",$item);
                if (count($arr2) == 2) {
                    list($title,$url) = $arr2;
                    $data[] = [
                        'title' => trim($title),
                        'url' => trim($url)
                    ];
                }
            }
        }
        return $data;
    }

    /**
     * 创建时间
     * @return bool|string
     */
    protected function setCreateTimeAttr()
    {
        return time();
    }
}