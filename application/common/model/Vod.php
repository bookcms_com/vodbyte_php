<?php
namespace app\common\model;
use org\Pinyin;
use think\Model;

class Vod extends Model
{
    protected $insert = ['vod_time','vod_skip_time'];

    /**
     * 反转义HTML实体标签
     * @param $value
     * @return string
     */
    protected function setContentAttr($value)
    {
        return htmlspecialchars_decode($value);
    }

    /**
     * 创建时间
     * @return bool|string
     */
    protected function setVodTimeAttr()
    {
        return time();
    }

    /**
     * 跳过时间
     * @return int
     */
    protected function setVodSkipTimeAttr()
    {
        return 0;
    }

}