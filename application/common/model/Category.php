<?php
namespace app\common\model;

use think\Db;
use think\Model;

class Category extends Model
{
    protected $insert = ['create_time'];

    /**
     * 自动生成时间
     * @return bool|string
     */
    protected function setCreateTimeAttr()
    {
        return time();
    }

    /**
     * 获取层级缩进列表数据
     * @return array
     */
    public function getLevelList()
    {
        $category_level = $this->order(['sort' => 'DESC'])->select();
        return array2level($category_level);
    }

    /**
     * 获取分类名称
     * @return array
     */
    public function getTypeList()
    {
        return $this->order(['sort' => 'DESC'])->column('id,name,pid','id');
    }
}
