<?php
namespace app\common\model;

use think\Db;
use think\Model;

class LiveArea extends Model
{
    protected $insert = ['create_time'];

    /**
     * 自动生成时间
     * @return bool|string
     */
    protected function setCreateTimeAttr()
    {
        return time();
    }

    /**
     * 获取层级缩进列表数据
     * @return array
     */
    public function getLevelList()
    {
        return $this->order(['sort' => 'DESC'])->select();
    }
}
