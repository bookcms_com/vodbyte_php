<?php
/**
 * Created by PhpStorm.
 * User: banyunba
 * Date: 2018/7/11
 * Time: 下午8:56
 */

namespace app\common\taglib;

use think\Db;
use think\facade\Config;
use think\facade\Url;
use think\template\TagLib;

class Tag extends TagLib
{
    /**
     * 定义标签列表
     */
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'web' => ['attr' => 'name', 'close' => 0],
        'slide' => ['attr' => 'limit'], //幻灯
        'nav_list' => ['attr' => 'limit'], //导航菜单
        'category_list' => ['attr' => 'pid,type,is_home,limit'],
        'help_category' => ['attr' => 'limit'],
        'help_list' => ['attr' => 'limit,affiche,cid'],
        'vip_list' => ['attr' => 'limit'],
        'random_doc_list' => ['attr' => 'cid,limit'],
        'article_list' => ['attr' => 'cid,subcid,is_recommend,field,orderby,limit,empty'],
        'last_update' => ['attr' => 'cid,limit'],
        'hot_update' => ['attr' => 'cid,limit'],
        'link' => ['attr' => 'limit', 'close' => 1],
        'content' => ['attr' => 'id,field,length', 'close' => 0],
        'banner' => ['attr' => 'id,limit'],
        'position' => ['close' => 0]
    ];


    /**
     * 随机文档
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagRandom_doc_list($tag, $content) {
        $cid = isset($tag['cid']) ? $tag['cid'] : 0;
        $limit = isset($tag['limit']) ? $tag['limit'] : 8;

        if ($cid != 0) {
            $cid  = $this->autoBuildVar($cid);
        }

        $parse = <<<EOF

        <?php

            \$__LIST__ = get_random_doc_list($cid,$limit);
            if(is_array(\$__LIST__)):
                foreach(\$__LIST__ as \$key => \$item):
            ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';
        $parse .= '<?php endif; ?>';
        return $parse;
    }

    /**
     * 导航菜单
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagNav_list($tag, $content)
    {
        $limit = isset($tag['limit']) ? $tag['limit'] : 5;

        $parse = <<<EOF
        <?php
               \$__LIST__ = think\Db::name('nav')->where(['status' => 1])->order(['sort' => 'DESC'])->limit('$limit')->select();

                foreach(\$__LIST__ as \$key => \$nav):
                    extract(\$nav);
                ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';

        return $parse;
    }


    /**
     * 幻灯片
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagSlide($tag, $content)
    {
        $limit = isset($tag['limit']) ? $tag['limit'] : 5;

        $parse = <<<EOF
        <?php
               \$__LIST__ = think\Db::name('slide')->where(['status' => 1])->order(['sort' => 'DESC'])->limit('$limit')->select();

                foreach(\$__LIST__ as \$key => \$slide):
                    extract(\$slide);
                ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';

        return $parse;
    }

    /**
     * 获取网站基本配置
     * @param $tag 参数
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function tagWeb($tag)
    {
        $name = isset($tag['name']) ? $tag['name'] : '';

        $site_config = Config::pull("site_config");
        if (array_key_exists($name,$site_config))
        {
            return $site_config[$name];
        }
        return "";
    }


    /**
     * 获取当前位置
     * @return string
     */
    public function tagPosition()
    {
        $parse = <<<EOF
        <?php
            \$iurl = url('index/index');
            \$position = "<a href='\$iurl'>首页</a>";
            /**分类**/
            if(isset(\$cid)){
                \$url = url('index/listing/index',['cid' => \$cate['id']]);
                if(isset(\$id)){
                    \$position .= "-><a href='\$url'>".\$cate['name']."</a>";
                }else{
                    \$position .= "->".\$cate['name'];
                }
                
            }
            /**资源**/
            if(isset(\$id)){
                \$position .= "->".msubstr(strip_tags(\$title),0,5);
            }
            echo \$position;
        ?>
EOF;
        return $parse;
    }

    /**
     * 获取单页分类某一字段的值
     * @param  [type] $tag [description]
     * @return [type]      [description]
     */
    public function tagContent($tag)
    {
        $id = $tag['id'];
        $field = $tag['field'];
        $length = isset($tag['length']) ? $tag['length'] : 0;
        $parse = <<<EOF
        <?php
                \$content = think\Db::name('category')->where(['id'=> $id])->value('$field');
                if ("$length" > 0){
                    \$content = msubstr(strip_tags(\$content),0,$length);
                }
                echo \$content;
                ?>
EOF;
        return $parse;
    }

    /**
     * 获取指定公告内容
     * @param  [type] $tag [description]
     * @return [type]      [description]
     */
    public function tagAnn($tag)
    {
        $id = $tag['id'];
        $length = isset($tag['length']) ? $tag['length'] : 0;
        $parse = <<<EOF
        <?php
                \$content = think\Db::name('flink')->where(['type' => 2,'id'=> $id])->value('description');
                if ("$length" > 0){
                    \$content = msubstr(strip_tags(\$content),0,$length);
                }
                echo \$content;
                ?>
EOF;
        return $parse;
    }

    /**
     * 获取导航列表
     */
    public function tagCategory_list($tag, $content)
    {
        $pid = isset($tag['pid']) ? $tag['pid'] : -1;
        $limit = isset($tag['limit']) ? $tag['limit'] : 0;
        $is_home = isset($tag['is_home']) ? $tag['is_home'] : 0;

        $parse = <<<EOF
        <?php
               \$__LIST__ = get_category_children("$pid","$limit","$is_home");

                foreach(\$__LIST__ as \$key => \$category):
                     extract(\$category);
                ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';
        return $parse;
    }

    //帮助分类
    public function tagHelp_category($tag, $content)
    {
        $limit = isset($tag['limit']) ? $tag['limit'] : 0;
        $is_home = isset($tag['is_home']) ? $tag['is_home'] : -1;

        $parse = <<<EOF
        <?php
          \$list = think\Db::name('help_category')->where(['status' => 1]);
                if ($is_home != -1){
                    \$list=\$list->where(['is_home'=> $is_home]);
                }
                
                if($limit != 0){
                    \$list=\$list->limit($limit);
                }
                \$__LIST__ = \$list->order(["sort" => "desc"])->select();

                foreach(\$__LIST__ as \$key => \$category):
                        extract(\$category);
                ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';
        return $parse;
    }

    /**
     * 帮助
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagHelp_list($tag, $content)
    {
        $cid = isset($tag['cid']) ? $tag['cid'] : 0;
        $limit = isset($tag['limit']) ? $tag['limit'] : 0;
        $affiche = isset($tag['affiche']) ? $tag['affiche'] : -1;
        $is_home = isset($tag['is_home']) ? $tag['is_home'] : -1;

        if ($cid != 0) {
            $cid  = $this->autoBuildVar($cid);
        }

        $parse = <<<EOF
        <?php
          \$list = think\Db::name('help')->where(['status' => 1]);
               
                if ($cid != 0){
                    \$list=\$list->where(['cid'=> $cid]);
                } 
                    
                if ($affiche != -1){
                    \$list=\$list->where(['affiche'=> $affiche]);
                } 
                 
                if ($is_home != -1){
                    \$list=\$list->where(['is_home'=> $is_home]);
                }
                
                if($limit != 0){
                    \$list=\$list->limit($limit);
                }
                \$__LIST__ = \$list->order(["sort" => "desc"])->select();

                foreach(\$__LIST__ as \$key => \$help):
                        extract(\$help);
                ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';
        return $parse;
    }

    /**
     * 收费列表
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagVip_list($tag, $content)
    {
        $limit = isset($tag['limit']) ? $tag['limit'] : 0;

        $parse = <<<EOF
        <?php
          \$list = think\Db::name('vip')->where(['status' => 1]);
               
                if($limit != 0){
                    \$list=\$list->limit($limit);
                }
                \$__LIST__ = \$list->order(["sort" => "asc"])->cache("vip_list",60)->select();

                foreach(\$__LIST__ as \$key => \$vip):
                        extract(\$vip);
                ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';
        return $parse;
    }


    /**
     * 获取热门
     */
    public function tagHot_update($tag, $content)
    {
        $cid = isset($tag['cid']) ? $tag['cid'] : 0;
        $limit = isset($tag['limit']) ? $tag['limit'] : 0;

        $parse = <<<EOF
        <?php
                \$list = think\Db::name('article')->where(['status' => 1]);
                if ($cid != 0){
                    \$list=\$list->where(['cid'=> $cid]);
                }
            
                if($limit != 0){
                    \$list=\$list->limit($limit);
                }
                \$list=\$list->order(["click" => "desc"])->select();

                \$__LIST__ = \$list;
                foreach(\$__LIST__ as \$key => \$article):
                        extract(\$article);
                ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';
        return $parse;
    }


    /**
     * 获取最近新增
     */
    public function tagLast_update($tag, $content)
    {
        $cid = isset($tag['cid']) ? $tag['cid'] : 0;
        $limit = isset($tag['limit']) ? $tag['limit'] : 0;

        $parse = <<<EOF
        <?php
                \$list = think\Db::name('article')->where(['status' => 1]);
                if ($cid != 0){
                    \$list=\$list->where(['cid'=> $cid]);
                }
            
                if($limit != 0){
                    \$list=\$list->limit($limit);
                }
                \$list=\$list->order(["create_time" => "desc"])->select();

                \$__LIST__ = \$list;
                foreach(\$__LIST__ as \$key => \$article):
                        extract(\$article);
                ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';
        return $parse;
    }

    /**
     * 获取內容列表
     */
    public function tagArticle_list($tag, $content)
    {
        $cid = isset($tag['cid']) ? $tag['cid'] : 0;
        $is_recommend = isset($tag['is_recommend']) ? $tag['is_recommend'] : -1;
        $order = isset($tag['orderby']) ? $tag['orderby'] : '';
        $limit = isset($tag['limit']) ? $tag['limit'] : 0;
        $subcid = isset($tag['subcid']) ? $tag['subcid'] : 0;
        $field = isset($tag['field']) ? $tag['field'] : '';

        if ($cid != 0) {
            $cid  = $this->autoBuildVar($cid);
        }
        if ($subcid != 0) {
            $subcid  = $this->autoBuildVar($subcid);
        }

        $parse = <<<EOF
        <?php
       
                \$list = think\Db::name('article')->where(['status' => 1]);

                if($cid != 0){
                    \$list = \$list->where(['cid' => $cid]);
                }  
                   
                if($subcid != 0){
                    \$cid_list = model('category')->where(['pid' => $subcid])->where(['status' => 1])->column('id');
                    \$list = \$list->whereIn('cid',$subcid);
                }
                    
                if($is_recommend != -1){
                    \$list = \$list->where(['is_recommend' => $is_recommend]);
                }
                
                if("$field" != ''){
                    \$list=\$list->field("$field");
                }

                if("$order" != ''){
                    \$list=\$list->order("$order");
                }

                if($limit != 0){
                    \$list=\$list->limit($limit);
                }
                \$list=\$list->select();

                \$__LIST__ = \$list;
                foreach(\$__LIST__ as \$key => \$article):
                      extract(\$article);
                ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach;?>';
        return $parse;
    }



    /**
     * 友情链接
     */
    public function tagLink($tag, $content)
    {
        $limit = isset($tag['limit']) ? $tag['limit'] : '';

        $parse = <<<EOF
        <?php
               \$__LIST__ = think\Db::name('link')->where(['status' => 1]);
               if("$limit" != ''){
                        \$__LIST__=\$__LIST__->limit($limit);
                    }
                \$__LIST__=\$__LIST__->cache()->select();

                foreach(\$__LIST__ as \$key => \$link):
                   extract(\$link);
                ?>
EOF;

        $parse .= $content;
        $parse .= '<?php endforeach; ?>';
        return $parse;
    }

}