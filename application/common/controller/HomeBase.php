<?php
namespace app\common\controller;

use think\facade\Cache;

class HomeBase extends CommonBase
{
    protected $select_vod_field = "id as vid,type_id as cid,vod_name,vod_sub,vod_en,vod_letter,vod_serial,vod_class,vod_remarks,vod_pic,vod_actor,vod_director,vod_blurb,vod_area,vod_lang,vod_year,vod_score,vod_custom_sort,vod_content,vod_play_from,vod_play_url,vod_play_note,vod_skip_time,vod_time";
    protected function initialize()
    {
        parent::initialize();
        if ((int)$this->siteConfig['api_cache'] == 0) {
            Cache::clear();
        }
    }


    /**
     * 获取首页数据
     * @param int $cid
     * @param string $extend
     * @return array
     */
    protected function get_base_home_data($cid,$extend,$name = '')
    {

        $item_list = [];
        $banner_list = [];

        if (!empty($extend)) {
            $extend = json_decode($extend,true);

            $class_name_list = explode(",",$extend['class']);
            foreach ($class_name_list as $class_name) {

                $classify_vod_list = model("vod")->where([['vod_class','like', "%{$class_name}%"],['vod_status','eq',1]])->field($this->select_vod_field)->order("sort DESC,vod_time DESC,id DESC")->limit(1,6)->select();
                foreach ($classify_vod_list as $key =>  $item) {
                    $classify_vod_list[$key]['vod_play_list'] = [];
                    if (!empty($item['vod_play_from'])) {
//                        $classify_vod_list[$key]['vod_play_list'] = mac_api_play_list($item['vod_play_from'], $item['vod_play_url'],$item['vod_play_note']);
                    }
                    unset($classify_vod_list[$key]['vod_play_from']);
                    unset($classify_vod_list[$key]['vod_play_note']);
                    unset($classify_vod_list[$key]['vod_play_url']);
                }
                if (!empty($name)) {
                    $class_name = sprintf("%s-%s",$name,$class_name);
                }
                $item_list[] = [
                    'title' => $class_name,
                    'vod_list' => $classify_vod_list,
                ];
            }

            if ($cid != 0) {
                $banner_list = model("vod")->where([
                    ['type_id','eq',$cid],
                    ['vod_status','eq',1],
                    ['vod_pic_slide','neq',""],
                    ['vod_level','eq',1]
                ])->field('id as vod_id,vod_name as title,vod_pic_slide as image_url')->order("sort DESC")->limit(30)->select();

                foreach ($banner_list as $key => $banner) {
                    $banner_list[$key]["vod"] = $this->get_item_detail($banner['vod_id']);
                    unset($banner_list[$key]["vod"]['vod_play_list']);
                }
            }

        }

        $data = [
            'banner_list' => $banner_list,
            'item_list' => $item_list,
        ];

        return $data;
    }

    protected function get_base_home_data2($cid,$offset = 0,$page_size = 0)
    {
        $banner_list = [];

        $vod_list = model("vod")->where([['type_id','eq',$cid],['vod_status','eq',1]])->field($this->select_vod_field)->order("sort DESC,vod_time DESC,id DESC")->limit($offset,$page_size)->select();

        foreach ($vod_list as $key =>  $item) {
            $vod_list[$key]['vod_play_list'] = [];
            if (!empty($item['vod_play_from'])) {
//                        $classify_vod_list[$key]['vod_play_list'] = mac_api_play_list($item['vod_play_from'], $item['vod_play_url'],$item['vod_play_note']);
            }
            unset($vod_list[$key]['vod_play_from']);
            unset($vod_list[$key]['vod_play_note']);
            unset($vod_list[$key]['vod_play_url']);
        }

        if ($cid != 0) {
            $banner_list = model("vod")->where([
                ['type_id','eq',$cid],
                ['vod_status','eq',1],
                ['vod_pic_slide','neq',""],
                ['vod_level','eq',1]
            ])->field('id as vod_id,vod_name as title,vod_pic_slide as image_url')->order("sort DESC")->limit(6)->select();

            foreach ($banner_list as $key => $banner) {
                $banner_list[$key]["vod"] = $this->get_item_detail($banner['vod_id']);
                unset($banner_list[$key]["vod"]['vod_play_list']);
            }
        }

        $data = [
            'banner_list' => $banner_list,
            'item_list' => $vod_list,
        ];

        return $data;
    }
    /**
     * 获取详情
     * @param int $vid
     * @return \think\Model
     */
    protected function get_item_detail($vid = 0,$type = '')
    {
        $vod = model("vod")->where(['vod_status' => 1,'id' => $vid])->field($this->select_vod_field)->find();

        $vod['vod_play_list'] = [];
        if (!empty($vod['vod_play_from'])) {
            $vod['vod_play_list'] = mac_api_play_list($vid,$vod['vod_play_from'], $vod['vod_play_url'],$vod['vod_play_note'],$vod['vod_custom_sort'],$type);
        }

        unset($vod['vod_play_from']);
        unset($vod['vod_play_note']);
        unset($vod['vod_play_url']);
        unset($vod['vod_custom_sort']);
        return $vod;
    }

}