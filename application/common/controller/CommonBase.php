<?php
/**
 * Created by PhpStorm.
 * User: banyunba
 * Date: 2018/5/15
 * Time: 下午12:21
 */

namespace app\common\controller;


use think\Controller;
use think\Db;
use think\facade\Cache;
use think\facade\Config;
use think\facade\Env;

class CommonBase extends Controller
{
    protected $siteConfig;

    protected function initialize()
    {
        parent::initialize();

        $this->siteConfig = Config::pull("site_config");
        $this->assign("site_info",$this->siteConfig);

//        $runtime_path = Env::get('runtime_path');
//        if ( delete_dir_file(	$runtime_path . 'temp/') ) {
//            Cache::clear();
//        }
    }


    /**
     * 输出json 数据
     * @param array $data
     * @param int $code
     * @param string $msg
     */
    protected function echoJson($data = array(),$code = 200,$msg = 'ok')
    {
        $data = array(
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        );
        header('Content-Type: application/json; charset=utf-8');
        exit(json_encode($data));
    }

    /**
     * 输出json 数据
     * @param array $data
     * @param int $code
     * @param string $msg
     */
    protected function echoJsonList($data = array(),$code = 0,$msg = 'ok')
    {
        $data = array(
            'code' => $code,
            'msg' => $msg,
            'count' => count($data),
            'data' => $data
        );
        header('Content-Type: application/json; charset=utf-8');
        exit(json_encode($data));
    }

}