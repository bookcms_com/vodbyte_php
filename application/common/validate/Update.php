<?php
namespace app\common\validate;

use think\Validate;

class Update extends Validate
{
    protected $rule = [
        'title' => 'require',
    ];

    protected $message = [
        'title.require' => '请输入标题',
    ];
}