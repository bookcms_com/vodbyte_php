<?php
namespace app\common\validate;

use think\Validate;

class Vod extends Validate
{
    protected $rule = [
        'type_id'   => 'require',
        'vod_name' => 'require',
        'sort'  => 'require|number'
    ];

    protected $message = [
        'type_id.require'   => '请选择所属分类',
        'vod_name.require' => '请输入标题',
        'sort.require'  => '请输入排序',
        'sort.number'   => '排序只能填写数字'
    ];
}