<?php
namespace app\admin\controller;

use ApkParser\Parser as ApkParser;
use app\common\model\Update as UpdateModel;
use app\common\controller\AdminBase;
use think\Exception;
use think\facade\Env;

/**
 * 更新管理
 * Class Live
 * @package app\admin\controller
 */
class Update extends AdminBase
{
    protected $model;

    protected function initialize()
    {
        parent::initialize();
        $this->model  = new UpdateModel();
    }

    /**
     * 內容管理
     * @param string $keyword 关键词
     * @param int    $page
     * @return mixed
     */
    public function index($keyword = '', $page = 1)
    {
        $update_list  = $this->model->order(['create_time' => 'DESC'])->paginate(15, false, ['page' => $page]);
        return $this->fetch('index', ['info_list' => $update_list, 'keyword' => $keyword]);
    }

    /**
     * 添加內容
     * @return mixed
     */
    public function add()
    {
        return $this->fetch();
    }

    /**
     * 保存內容
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            $validate_result = $this->validate($data, 'Update');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {

                try {
                    $file_path = Env::get('root_path') . 'public/' . $data['url'];
                    $data['md5'] = md5_file($file_path);
                    $apk = new ApkParser($file_path);
                    $manifest = $apk->getManifest();
                    $data['package_name'] = $manifest->getPackageName();
                    $data['ver_name'] = $manifest->getVersionName();
                    $data['ver_code'] = $manifest->getVersionCode();
                }catch (Exception $e) {
                    $this->error($e->getMessage());
                }

                if ($this->model->allowField(true)->save($data)) {
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑內容
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $info = $this->model->find($id);
        return $this->fetch('edit', ['info' => $info]);
    }

    /**
     * 更新內容
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            $validate_result = $this->validate($data, 'Update');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                if ($this->model->allowField(true)->save($data, $id) !== false) {
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }
            }
        }
    }

    /**
     * 删除內容
     * @param int   $id
     * @param array $ids
     */
    public function delete($id = 0, $ids = [])
    {
        $id = $ids ? $ids : $id;
        if ($id) {
            if ($this->model->destroy($id)) {
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('请选择需要删除的內容');
        }
    }

}