<?php
namespace app\admin\controller;

use app\common\model\Feedback as FeedbackModel;
use app\common\controller\AdminBase;

/**
 * 反馈管理
 * Class Article
 * @package app\admin\controller
 */
class Feedback extends AdminBase
{
    protected $article_model;

    protected function initialize()
    {
        parent::initialize();
        $this->article_model  = new FeedbackModel();
    }

    /**
     * 內容管理
     * @param int    $page
     * @return mixed
     */
    public function index(  $page = 1)
    {
        $info_list  = $this->article_model->order(['create_time' => 'DESC'])->paginate(15, false, ['page' => $page]);
        return $this->fetch('index', ['info_list' => $info_list]);
    }

    /**
     * 删除內容
     * @param int   $id
     * @param array $ids
     */
    public function delete($id = 0, $ids = [])
    {
        $id = $ids ? $ids : $id;
        if ($id) {
            if ($this->article_model->destroy($id)) {
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('请选择需要删除的內容');
        }
    }

}