<?php
namespace app\admin\controller;

use app\common\controller\AdminBase;
use think\facade\Cache;
use think\facade\Env;

/**
 * 系统配置
 * Class System
 * @package app\admin\controller
 */
class System extends AdminBase
{
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * 站点配置
     */
    public function siteConfig()
    {
        return $this->fetch('site_config');
    }

    /**
     * 更新配置
     */
    public function updateSiteConfig()
    {
        if ($this->request->isPost()) {
            $site_config = $this->request->post('site_config/a');

            if (array_key_exists("vod_inrule",$site_config)) {
                $site_config['vod_inrule'] = implode(',',$site_config['vod_inrule']);
            }
            if (array_key_exists("vod_uprule",$site_config)) {
                $site_config['vod_uprule'] = implode(',',$site_config['vod_uprule']);
            }

            $file_path = Env::get('app_path') . "config/site_config.php";

            if (arr2file($file_path,$site_config)) {
                Cache::clear();
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        }
    }

    /**
     * 邮件服务
     * @return mixed
     */
    public function email ()
    {
        return $this->fetch();
    }

    /**
     * 清除缓存
     */
    public function clear()
    {
        if (Cache::clear()) {
            $this->success('清除缓存成功');
        } else {
            $this->error('清除缓存失败');
        }
    }
}