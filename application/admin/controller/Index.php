<?php
namespace app\admin\controller;

use app\common\controller\AdminBase;
use think\Db;

/**
 * 后台首页
 * Class Index
 * @package app\admin\controller
 */
class Index extends AdminBase
{
    protected function initialize()
    {
        parent::initialize();
    }

    /**
     * 首页
     * @return mixed
     */
    public function index()
    {
        $version = Db::query('SELECT VERSION() AS ver');
        $config  = [
            'url'             => $_SERVER['HTTP_HOST'],
            'document_root'   => $_SERVER['DOCUMENT_ROOT'],
            'server_os'       => PHP_OS,
            'server_port'     => $_SERVER['SERVER_PORT'],
            'server_soft'     => $_SERVER['SERVER_SOFTWARE'],
            'php_version'     => PHP_VERSION,
            'mysql_version'   => $version[0]['ver'],
            'max_upload_size' => ini_get('upload_max_filesize')
        ];

        return $this->fetch('index', ['config' => $config]);
    }


    public function console ()
    {
        $user_limit = model("user")->where(['status' => 1])->count();
        $article_limit = model("vod")->where(['vod_status' => 1])->count();
        $vip_limit = model("user")->where('vip_id','gt',1)->count();
        $feedback_limit = model("feedback")->where(['status' => 1])->count();

        return $this->fetch('console',['feedback_limit' => $feedback_limit,'user_limit' => $user_limit,'vip_limit' => $vip_limit,'article_limit' => $article_limit]);
    }

    /**
     * 搜索排行
     */
    public function top_search(){
        $search_list_key = get_redis_cid_ranking_list(10,"search");

        $data_list = [];
        foreach ($search_list_key as $word => $value) {
            $data_list[] = [
                'word' => $word,
                'click' => (int)$value,
                'like_count' => model("vod")->where('vod_name','like', "%{$word}%")->count(),
                'eq_count' => model("vod")->where('vod_name','eq', $word)->count(),
            ];
        }

        $this->echoJsonList($data_list);
    }

    /**
     * 今日热搜
     */
    public function day_top_hot(){
        $vod_list = get_top_vod_data_list(10);
        $this->echoJsonList($vod_list);
    }

    public function select()
    {
        $tpl = $this->request->param('tpl');
        $tab = $this->request->param('tab');
        $col = $this->request->param('col');
        $refresh = $this->request->param('refresh');
        $ids = $this->request->param('ids');
        $url = $this->request->param('url');
        $val = $this->request->param('val');

        if(empty($tpl) || empty($tab) || empty($col) || empty($ids) || empty($url)){
            return $this->error('参数错误');
        }

        if(is_array($ids)){
            $ids = join(',',$ids);
        }

        if(empty($refresh)){
            $refresh = 'yes';
        }

        $url = url($url);
        $mid = 1;
        if($tab=='art'){
            $mid = 2;
        }
        $this->assign('mid',$mid);

        if($tpl=='select_type'){
            $type_tree = model('category')->getLevelList();
            $this->assign('type_tree',$type_tree);
        }
        elseif($tpl =='select_level'){
            $level_list = [1,2,3,4,5,6,7,8,9];
            $this->assign('level_list',$level_list);
        }
        elseif($tpl =='select_levels'){
            $level_list = [1];
            $this->assign('levels_list',$level_list);
        }

        $this->assign('refresh',$refresh);
        $this->assign('url',$url);
        $this->assign('tab',$tab);
        $this->assign('col',$col);
        $this->assign('ids',$ids);
        $this->assign('val',$val);
        return $this->fetch( 'public/'.$tpl);
    }
}