<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use think\Db;

class Database extends AdminBase
{

    public function replace()
    {
        $table ="sd_vod";

        if($this->request->isPost()){

            $field = $this->request->param('field');
            $findstr = $this->request->param('findstr');
            $tostr = $this->request->param('tostr');
            $where = $this->request->param('where');

            if(!empty($table) && !empty($field) && !empty($findstr) && !empty($tostr)){
                $sql = "UPDATE ".$table." set ".$field."=Replace(".$field.",'".$findstr."','".$tostr."') where 1=1 ". $where;
                Db::execute($sql);
                return $this->success("执行成功");
            }else {
                return $this->error("参数错误");
            }

        }else {
            $list = Db::query('SHOW COLUMNS FROM ' . $table);
            return $this->fetch('replace',['list' => $list]);
        }
    }
}
