<?php
namespace app\admin\controller;

use app\common\model\Vod as ArticleModel;
use app\common\model\Category as CategoryModel;
use app\common\controller\AdminBase;

/**
 * 分类管理
 * Class Category
 * @package app\admin\controller
 */
class Category extends AdminBase
{

    protected $category_model;
    protected $article_model;

    protected function initialize()
    {
        parent::initialize();
        $this->category_model = new CategoryModel();
        $this->article_model  = new ArticleModel();
        $category_level_list  = $this->category_model->getLevelList();
        $this->assign('category_level_list', $category_level_list);
    }

    /**
     * 分类管理
     * @return mixed
     */
    public function index()
    {
        return $this->fetch();
    }

    /**
     * 添加分类
     * @param string $pid
     * @return mixed
     */
    public function add($pid = '')
    {
        return $this->fetch('add', ['pid' => $pid]);
    }

    /**
     * 保存分类
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            $validate_result = $this->validate($data, 'Category');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                if(!empty($data['extend'])){
                    $data['extend'] = json_encode($data['extend']);
                }

                if ($this->category_model->allowField(true)->save($data)) {
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }

            }
        }
    }

    /**
     * 编辑分类
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $category = $this->category_model->find($id);
        if(!empty($category['extend'])){
            $category['extend'] = json_decode($category['extend'],true);
        }
        return $this->fetch('edit', ['category' => $category]);
    }

    /**
     * 更新分类
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            $validate_result = $this->validate($data, 'Category');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                if(!empty($data['extend'])){
                    $data['extend'] = json_encode($data['extend']);
                }
                if ($this->category_model->allowField(true)->save($data, $id) !== false) {
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }
            }
        }
    }

    /**
     * 删除分类
     * @param $id
     */
    public function delete($id)
    {
        if ($this->category_model->destroy($id)) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }
}