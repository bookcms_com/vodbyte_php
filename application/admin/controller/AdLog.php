<?php
namespace app\admin\controller;

use app\common\model\AdLog as AdLogModel;
use app\common\controller\AdminBase;

/**
 * 广告日志管理
 * Class Live
 * @package app\admin\controller
 */
class AdLog extends AdminBase
{
    protected $article_model;

    protected function initialize()
    {
        parent::initialize();
        $this->article_model  = new AdLogModel();
    }

    /**
     * 內容管理
     * @param string $keyword 关键词
     * @param int    $page
     * @return mixed
     */
    public function index($keyword = '', $page = 1)
    {
        $map   = [];

        if (!empty($keyword)) {
            $map[] = ['title','like', "%{$keyword}%"];
        }

        $article_list  = $this->article_model->where($map)->order(['create_time' => 'DESC'])->paginate(15, false, ['page' => $page]);

        return $this->fetch('index', ['article_list' => $article_list, 'keyword' => $keyword]);
    }

    /**
     * 删除內容
     * @param int   $id
     * @param array $ids
     */
    public function delete($id = 0, $ids = [])
    {
        $id = $ids ? $ids : $id;
        if ($id) {
            if ($this->article_model->destroy($id)) {
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('请选择需要删除的內容');
        }
    }

}