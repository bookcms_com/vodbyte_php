<?php
namespace app\admin\controller;

use app\common\controller\AdminBase;
use think\facade\Env;

/**
 * 通用上传接口
 * Class Upload
 * @package app\api\controller
 */
class Upload extends AdminBase
{

    /**
     * 上传图片
     * @return \think\response\Json
     */
    public function upload()
    {
        if ($this->request->isPost())
        {
            $config = [
                'size' => 209715200 //最大上传200MB
            ];

            $file = $this->request->file('file');

            $upload_path = str_replace('\\', '/', Env::get('root_path') . 'public/uploads');
            $info        = $file->validate($config)->move($upload_path);

            $fileUrl = str_replace('\\', '/', "uploads/" . $info->getSaveName());

            if ($info) {
                $result = [
                    'code' => 0,
                    'url'   => $fileUrl
                ];
            } else {
                $result = [
                    'code'   => 1,
                    'msg' => $file->getError()
                ];
            }

            return json($result);
        }


    }

}