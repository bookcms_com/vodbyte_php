<?php
namespace app\admin\controller;

use app\common\model\LiveArea as LiveAreaModel;
use app\common\controller\AdminBase;

/**
 * 地区管理
 * Class LiveArea
 * @package app\admin\controller
 */
class LiveArea extends AdminBase
{
    protected $category_model;

    protected function initialize()
    {
        parent::initialize();
        $this->category_model  = new LiveAreaModel();
        $category_level_list  = $this->category_model->getLevelList();
        $this->assign('category_level_list', $category_level_list);
    }

    /**
     * 分类管理
     * @return mixed
     */
    public function index()
    {
        return $this->fetch();
    }

    /**
     * 添加分类
     * @param string $pid
     * @return mixed
     */
    public function add($pid = '')
    {
        return $this->fetch('add', ['pid' => $pid]);
    }

    /**
     * 保存分类
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            $validate_result = $this->validate($data, 'LiveArea');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {

                if ($this->category_model->allowField(true)->save($data)) {
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }

            }
        }
    }

    /**
     * 编辑分类
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $category = $this->category_model->find($id);

        return $this->fetch('edit', ['category' => $category]);
    }

    /**
     * 更新分类
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            $validate_result = $this->validate($data, 'LiveArea');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                if ($this->category_model->allowField(true)->save($data, $id) !== false) {
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }
            }
        }
    }

    /**
     * 删除分类
     * @param $id
     */
    public function delete($id)
    {
        if ($this->category_model->destroy($id)) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }
}