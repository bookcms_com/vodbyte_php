<?php
namespace app\admin\controller;

use app\common\controller\CommonBase;
use think\Db;
use think\facade\Config;
use think\facade\Request;
use think\facade\Session;

/**
 * 后台登录
 * Class Login
 * @package app\admin\controller
 */
class Login extends CommonBase
{
    protected function initialize()
    {
        parent::initialize();
        $__bind_domain = Config::pull("bind_domain");
        if ($__bind_domain['domain'] != Request::host()) {
            return $this->redirect("http://www.baidu.com");
        }

    }

    /**
     * 后台登录
     * @return mixed
     */
    public function index()
    {
        return $this->fetch();
    }

    /**
     * 登录验证
     */
    public function login()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->only(['username', 'password', 'verify']);
            $validate_result = $this->validate($data, 'Login');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $where['username'] = $data['username'];
                $where['password'] = md5($data['password'] . Config::get('salt'));

                $admin_user = Db::name('admin_user')->field('id,username,status')->where($where)->find();

                if (!empty($admin_user)) {
                    if ($admin_user['status'] != 1) {
                        $this->echoJson('',301,"当前用户已禁用!");
                    } else {
                        Session::set('admin_id', $admin_user['id']);
                        Session::set('admin_name', $admin_user['username']);
                        Db::name('admin_user')->update(
                            [
                                'last_login_time' => date('Y-m-d H:i:s', time()),
                                'last_login_ip'   => $this->request->ip(),
                                'id'              => $admin_user['id']
                            ]
                        );
                        $this->echoJson('',200,"登录成功!");
                    }
                } else {
                    $this->echoJson('',404,"用户名或密码错误");
                }
            }
        }
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        Session::delete('admin_id');
        Session::delete('admin_name');
        $this->echoJson('',200,"退出成功");
    }
}
