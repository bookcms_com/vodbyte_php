<?php
namespace app\admin\controller;

use app\common\model\Help as HelpModel;
use app\common\model\HelpCategory as CategoryModel;
use app\common\controller\AdminBase;

/**
 * 帮助分类管理
 * Class Category
 * @package app\admin\controller
 */
class HelpCategory extends AdminBase
{

    protected $category_model;
    protected $article_model;

    protected function initialize()
    {
        parent::initialize();
        $this->category_model = new CategoryModel();
        $this->article_model  = new HelpModel();
        $category_list  = $this->category_model->getLevelList();

        $this->assign('category_list', $category_list);
    }

    /**
     * 分类管理
     * @return mixed
     */
    public function index()
    {
        return $this->fetch();
    }

    /**
     * 添加分类
     * @return mixed
     */
    public function add()
    {
        return $this->fetch('add');
    }

    /**
     * 保存分类
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            $validate_result = $this->validate($data, 'HelpCategory');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                if (empty($data['alias'])) {
                    $pinyinFirst = Chinese::toPinyin($data['name'], Pinyin::CONVERT_MODE_PINYIN_FIRST | Pinyin::CONVERT_MODE_PINYIN_SOUND_NUMBER, '');
                    $data['alias'] = $pinyinFirst['pinyinFirst'][0];
                }

                if ($this->category_model->allowField(true)->save($data)) {
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑分类
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $category = $this->category_model->find($id);

        return $this->fetch('edit', ['category' => $category]);
    }

    /**
     * 更新分类
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            $validate_result = $this->validate($data, 'HelpCategory');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {

                if ($this->category_model->allowField(true)->save($data, $id) !== false) {
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }

            }
        }
    }

    /**
     * 删除分类
     * @param $id
     */
    public function delete($id)
    {
        $article  = $this->article_model->where(['cid' => $id])->find();

        if (!empty($article)) {
            $this->error('此分类下存在內容，不可删除');
        }
        if ($this->category_model->destroy($id)) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }
}