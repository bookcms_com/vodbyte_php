<?php
namespace app\admin\controller;

use app\common\model\Vod as VodModel;
use app\common\model\Category as CategoryModel;
use app\common\controller\AdminBase;
use org\Pinyin;

/**
 * 內容管理
 * Class Article
 * @package app\admin\controller
 */
class Vod extends AdminBase
{
    protected $vod_model;
    protected $category_model;

    protected function initialize()
    {
        parent::initialize();
        $this->vod_model  = new VodModel();
        $this->category_model = new CategoryModel();

        $category_list = $this->category_model->getTypeList();
        $this->assign('category_list', $category_list);
    }

    /**
     * 內容管理
     * @param int    $cid     分类ID
     * @param string $keyword 关键词
     * @param int    $page
     * @return mixed
     */
    public function index($cid = 0, $keyword = '', $page = 1)
    {
        $map   = [];
        $keyword = trim($keyword);

        if (!empty($keyword)) {
            $map[] = ['vod_name','like', "%{$keyword}%"];
        }

        $vod_list  = $this->vod_model->where($map)->order(['id' => 'DESC'])->paginate(15, false, ['page' => $page]);
        return $this->fetch('index', ['vod_list' => $vod_list, 'cid' => $cid, 'keyword' => $keyword]);
    }

    /**
     * 添加內容
     * @return mixed
     */
    public function add()
    {
        return $this->fetch();
    }

    /**
     * 保存內容
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            $validate_result = $this->validate($data, 'Vod');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {

                $data['vod_first_letter'] = Pinyin::get($data['vod_name'],"first");

                if(empty($data['vod_en'])){
                    $data['vod_en'] = Pinyin::get($data['vod_name']);
                }

                if(empty($data['vod_letter'])){
                    $data['vod_letter'] = strtoupper(substr($data['vod_en'],0,1));
                }

                if(empty($data['vod_blurb'])){
                    $data['vod_blurb'] = mac_substring( strip_tags($data['vod_content']) ,100);
                }

                if(empty($data['vod_play_url'])){
                    $data['vod_play_url'] = '';
                }

                if(!empty($data['vod_play_from'])) {
                    $data['vod_play_from'] = join('$$$', $data['vod_play_from']);
                    $data['vod_play_note'] = join('$$$', $data['vod_play_note']);
                    $data['vod_play_url'] = join('$$$', $data['vod_play_url']);
                    $data['vod_play_url'] = str_replace( array(chr(10),chr(13)), array('','#'),$data['vod_play_url']);
                } else{
                    $data['vod_play_from'] = '';
                    $data['vod_play_note'] = '';
                    $data['vod_play_url'] = '';
                }

                $data['vod_play_url'] = htmlspecialchars_decode($data['vod_play_url']);
                $data['vod_content'] = htmlspecialchars_decode($data['vod_content']);

                if ($this->vod_model->allowField(true)->save($data)) {
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑內容
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $info = $this->vod_model->find($id);

        $info['vod_play_list'] = [];
        if (!empty($info['vod_play_from'])) {
            $info['vod_play_list'] = mac_play_list($info['vod_play_from'], $info['vod_play_url'],$info['vod_play_note']);
        }

        return $this->fetch('edit', ['info' => $info]);
    }

    /**
     * 更新內容
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->param();
            $validate_result = $this->validate($data, 'Vod');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {

                if(empty($data['vod_en'])){
                    $data['vod_en'] = Pinyin::get($data['vod_name']);
                }

                if(empty($data['vod_first_letter'])){
                    $data['vod_first_letter'] = Pinyin::get($data['vod_name'],"first");
                }

                if(empty($data['vod_letter'])){
                    $data['vod_letter'] = strtoupper(substr($data['vod_en'],0,1));
                }

                if(empty($data['vod_blurb'])){
                    $data['vod_blurb'] = mac_substring( strip_tags($data['vod_content']) ,100);
                }

                if(empty($data['vod_play_url'])){
                    $data['vod_play_url'] = '';
                }

                if(!empty($data['vod_play_from'])) {
                    $data['vod_play_from'] = join('$$$', $data['vod_play_from']);
                    $data['vod_play_note'] = join('$$$', $data['vod_play_note']);
                    $data['vod_play_url'] = join('$$$', $data['vod_play_url']);
                    $data['vod_play_url'] = str_replace( array(chr(10),chr(13)), array('','#'),$data['vod_play_url']);
                } else{
                    $data['vod_play_from'] = '';
                    $data['vod_play_note'] = '';
                    $data['vod_play_url'] = '';
                }

                $data['vod_play_url'] = htmlspecialchars_decode($data['vod_play_url']);
                $data['vod_content'] = htmlspecialchars_decode($data['vod_content']);

                $data['vod_time'] = time();
                if ($this->vod_model->allowField(true)->save($data, $id) !== false) {
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }
            }
        }
    }

    /**
     * 删除內容
     * @param int   $id
     * @param array $ids
     */
    public function delete($id = 0, $ids = [])
    {
        $id = $ids ? $ids : $id;
        if ($id) {
            if ($this->vod_model->destroy($id)) {
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('请选择需要删除的內容');
        }
    }

    /**
     * 內容审核状态切换
     * @param array  $ids
     * @param string $type 操作类型
     */
    public function toggle($ids = [], $type = '')
    {
        $data   = [];
        $status = $type == 'audit' ? 1 : 0;

        if (!empty($ids)) {
            foreach ($ids as $value) {
                $data[] = ['id' => $value, 'vod_status' => $status];
            }
            if ($this->vod_model->saveAll($data)) {
                $this->success('操作成功');
            } else {
                $this->error('操作失败');
            }
        } else {
            $this->error('请选择需要操作的內容');
        }
    }
}