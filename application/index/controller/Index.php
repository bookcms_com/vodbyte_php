<?php
namespace app\index\controller;

use app\common\controller\HomeBase;
use think\Controller;
use think\facade\Cache;
use think\facade\Config;

class Index extends Controller
{

    protected function initialize()
    {
        parent::initialize();
    }

    public function index ()
    {
        return $this->redirect("http://www.baidu.com");
    }

}