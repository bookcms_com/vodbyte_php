<?php
namespace app\tv\controller;

use app\common\controller\HomeBase;
use think\facade\Cache;
use think\facade\Config;

class V1 extends HomeBase
{

    public function config()
    {

        $affiche_list = model('help')->where(['cid' => 49])->field('id,title,content')->cache()->select();
        $config = Config::pull("site_config");

        $data = [
            'pic_domain' => $config['pic_domain'],
            'play_note_info' => $config['play_note_info'],
            'open_tv_video_decode' => $config['open_tv_video_decode'],
            'affiche_list' => $affiche_list,
            'open_tv_ad' => $config['open_tv_ad'],
            'tv_ad_url' => $config['tv_ad_url'],
            'search_qrcode_url' => $config['search_qrcode_url'],
            'error_qrcode_url' => $config['error_qrcode_url'],
            'tv_ad_jump_url' => $config['tv_ad_jump_url'],
        ];

        $this->echoJson($data);
    }


    /**
     * 首页数据
     */
    public function home_data_list ()
    {
        $cid = (int)$this->request->param("cid",0);
        $home_style = (int)$this->request->param("home_style",0);
        $page = (int)$this->request->param("page",1);

        $page_size = 12;
        $offset = ($page - 1) * $page_size;

        $cache_key = md5("tv_home_data_list_" . $cid . $home_style . $page);
        if (Cache::has($cache_key)) {
            $data = Cache::get($cache_key);
        }else {

            if ($cid == 0){
                $category_list = model("category")->where(['is_home' => 1,'status' => 1])->order("sort DESC")->field(['id','name','extend'])->cache()->select();

                $item_list = [];
                foreach ($category_list as $category){
                    $data = $this->get_base_home_data($category['id'],$category['extend'],$category['name']);
                    $item_list = array_merge($item_list,$data['item_list']);
                }

                $banner_list = model("vod")->where([
                    ['vod_status','eq',1],
                    ['vod_pic_slide','neq',""],
                    ['vod_level','eq',2]
                ])->field('id as vod_id,vod_name as title,vod_pic_slide as image_url')->order("sort DESC")->limit(6)->select();

                foreach ($banner_list as $key => $banner) {
                    $banner_list[$key]["vod"] = $this->get_item_detail($banner['vod_id']);
                    unset($banner_list[$key]["vod"]['vod_play_list']);
                }

                $data = [
                    'banner_list' => $banner_list,
                    'item_list' => $item_list,
                ];

            }else {
                if ($home_style == 0) {
                    $category = model("category")->where(['is_home' => 1,'status' => 1,'id' => $cid])->order("sort DESC")->field(['id','extend'])->cache()->find();
                    $data = $this->get_base_home_data($cid,$category['extend']);
                }else {
                    $data = $this->get_base_home_data2($cid,$offset,$page_size);
                }
            }
            Cache::set($cache_key,$data);
        }

        $this->echoJson($data);
    }


    /**
     * 电视直播列表
     */
    public function live_list()
    {
        $cache_key = md5("tv_live_list");
        if (Cache::has($cache_key)) {
            $area_list = Cache::get($cache_key);
        }else {
            $area_list = model("live_area")->where(['status' => 1])->field(['id','title'])->select();
            foreach ($area_list as $key => $area) {
                $live  = model("live")->where(['status' => 1,'cid' => $area['id']])->field(['image','title','content'])->order("sort DESC")->select();
                $area_list[$key]['data'] = $live;
            }
            Cache::set($cache_key,$area_list);
        }

        $this->echoJson($area_list);
    }


    /**
     * 分类列表数据
     */
    public function classify_vod_list()
    {
        $page = $this->request->param('page',1);
        $cid = $this->request->param('cid',0);
        $vod_class = $this->request->param('vod_class',"all");
        $area = $this->request->param('area',"all");
        $lang = $this->request->param('lang',"all");
        $year = $this->request->param('year',0);

        $page_size = 12;
        $offset = ($page - 1) * $page_size;

        $where = [];
        $where[] = ['vod_status','eq',1];
        $where[] = ['type_id','eq',$cid];

        //分类
        if ($vod_class != 'all')
        {
            $where[] = ['vod_class','like', "%{$vod_class}%"];
        }
        // 语言
        if ($lang != 'all')
        {
            $where[] = ['vod_lang','like', "%{$lang}%"];
        }
        // 年代
        if ($year != 0)
        {
            $where[] = ['vod_year','eq',$year];
        }
        // 地区
        if ($area != 'all')
        {
            $where[] = ['vod_area','like', "%{$area}%"];
        }

        $cache_key = md5(sprintf("tv_classify_vod_list_%d-%d-%s-%s-%s-%s",$page,$cid,$vod_class,$area,$lang,$year));

        if (Cache::has($cache_key)) {
            $classify_vod_list = Cache::get($cache_key);
        }else {
            $classify_vod_list = model("vod")->where($where)->field($this->select_vod_field)->order("sort DESC,vod_time DESC,id DESC")->limit($offset,$page_size)->select();
            foreach ($classify_vod_list as $key =>  $item) {
                $classify_vod_list[$key]['vod_play_list'] = [];
                if (!empty($item['vod_play_from'])) {
//                $classify_vod_list[$key]['vod_play_list'] = mac_api_play_list($item['vod_play_from'], $item['vod_play_url'],$item['vod_play_note']);
                }
                unset($classify_vod_list[$key]['vod_play_from']);
                unset($classify_vod_list[$key]['vod_play_note']);
                unset($classify_vod_list[$key]['vod_play_url']);
            }
            Cache::set($cache_key,$classify_vod_list);
        }

        $this->echoJson($classify_vod_list);
    }

    /**
     * 分类标题
     */
    public function classify_title()
    {

        $is_home = (int)$this->request->param("is_home",0);
        $cache_key = md5('tv_classify_title_list_' . $is_home);

        if ($is_home == 1) {
            $where[] = ['is_home','eq',$is_home];
        }

        $where[] = ['status','eq',1];

        if (Cache::has($cache_key)) {
            $classify_title_list = Cache::get($cache_key);
        }else {
            $classify_title_list = model("category")->where($where)->order("sort DESC")->field(['id','name','extend'])->cache(true,3600)->select();
            foreach ($classify_title_list as $key => $item) {
                if (!empty($item['extend'])) {
                    $extend = json_decode($item['extend'],true);
                    $classify_title_list[$key]['extend'] = [
                        'class_name' => explode(",",$extend['class']),
                        'area' => explode(",",$extend['area']),
                        'lang' => explode(",",$extend['lang']),
                        'year' => explode(",",$extend['year'])
                    ];
                }
            }
            Cache::set($cache_key,$classify_title_list);
        }

        $this->echoJson($classify_title_list);
    }

    /**
     * 视频详情
     */
    public function vod_item_detail()
    {
        $vid = $this->request->param("vid",0);
        if ($vid == 0) {
            $this->echoJson([]);
        }
        $cache_key = md5('tv_vod_item_detail_' . $vid);
        if (Cache::has($cache_key)) {
            $vod = Cache::get($cache_key);
        }else {
            $vod = $this->get_item_detail($vid,'tv');
            Cache::set($cache_key,$vod);
        }

        $this->echoJson($vod);
    }


    /**
     * 相关视频
     */
    public function vod_item_detail_relevance_list()
    {
        $cid = $this->request->param("cid",0);
        $vid = $this->request->param("vid",0);

        if ($cid == 0) {
            $this->echoJson([]);
        }

        $cache_key = md5('tv_vod_item_detail_relevance_list_' . $cid);
        if (Cache::has($cache_key)) {
            $data = Cache::get($cache_key);
        }else {
            $data = [];
            $list = model("vod")->where(['vod_status' => 1,'type_id' => $cid])->orderRaw('rand()')->limit(3)->field('id,type_id as cid')->select();
            foreach ($list as $item) {
                $data[] = $this->get_item_detail($item['id']);
            }

            Cache::set($cache_key,$data);
        }

        $this->echoJson($data);
    }

    public function get_search()
    {
        $word = strtolower($this->request->param("word",""));
        $page = (int)$this->request->param("page",1);

        if (empty($word)) {
            $this->echoJson([]);
        }

        $page_size = 12;
        $offset = ($page - 1) * $page_size;

        $cache_key = md5('tv_get_search_' . $word);
        if (Cache::has($cache_key)) {
            $data = Cache::get($cache_key);
        }else {
            $data = [];

            $list = model("vod")->where([['vod_status','eq', 1],['vod_first_letter','like', "%{$word}%"]])->whereLike("vod_en", "%{$word}%","or")->limit($offset,$page_size)->select();
            foreach ($list as $item) {
                $data[] = $this->get_item_detail($item['id']);
            }

            Cache::set($cache_key,$data);
        }

        $this->echoJson($data);
    }


    /**
     * 获取热门搜索
     */
    public function get_hot_search()
    {
        $config = Config::pull("site_config");
        $list = explode("\r",$config['hot_search_word']);
        return $this->echoJson($list);
    }

    /**
     * 清空缓存
     */
    public function clear_cache()
    {
        Cache::clear();
        $this->echoJson("清除成功");
    }

    public function feedback()
    {
        $data = $this->request->only(["vid","uuid","content"]);
        $data['status'] = 1;

        $md5 = md5($data['vid'].$data['uuid'].$data['content']);
        $res = model('feedback')->where(['md5' => $md5])->find();
        $data['md5'] = $md5;

        if ($res) {
            $this->echoJson("请勿重复提交");
        }else {
            $res = model('feedback')->save($data);
            if ($res) {
                $this->echoJson("反馈成功");
            }else {
                $this->echoJson("反馈失败");
            }
        }
    }

}
