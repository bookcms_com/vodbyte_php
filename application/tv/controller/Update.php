<?php
namespace app\tv\controller;

use app\common\controller\HomeBase;
use think\facade\Config;
use think\facade\Request;

class Update extends HomeBase
{
    /**
     * 电视版
     */
    public function index()
    {
        $only = $this->request->only(['ver_name','ver_code']);

        if (empty($only)) {
            return;
        }
        $info = model('update')->where(['app_type' => 'tv'])->order("id DESC")->find();
        $data = [
            'title' => "",
            'url' => "",
            'content' => "",
        ];

        if(version_compare( $only['ver_code'],$info['ver_code'],'<'))
        {
            if (substr($info["url"], 0, 4) != "http") {
                $url =  Request::domain() . '/' . $info['url'];
            }else {
                $url =  $info['url'];
            }

            $data = [
                'title' => $info['title'],
                'url' => $url,
                'content' => $info['content'],
            ];

            $this->echoJson($data);
        }else {
            $this->echoJson($data,-1);
        }
    }

}
