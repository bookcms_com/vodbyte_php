SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sd_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `sd_admin_user`;
CREATE TABLE `sd_admin_user`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户名',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员密码',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态 1 启用 0 禁用',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后登录IP',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sd_admin_user
-- ----------------------------
INSERT INTO `sd_admin_user` VALUES (1, 'admin', '7b79e788e94c1c41b6d4e1b1280c4bdb', 1, '2016-10-18 15:28:37', '2021-06-13 11:09:16', '127.0.0.1');

-- ----------------------------
-- Table structure for sd_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `sd_auth_group`;
CREATE TABLE `sd_auth_group`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限规则ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sd_auth_group
-- ----------------------------
INSERT INTO `sd_auth_group` VALUES (1, '超级管理组', 1, '1,2,3,73,74,5,6,7,8,9,10,11,12,39,40,41,42,43,14,13,20,21,22,23,24,15,25,26,27,28,29,30,16,17,44,45,46,47,48,18,49,50,51,52,53,19,31,32,33,34,35,36,37,54,55,58,59,60,61,62,56,63,64,65,66,67,57,68,69,70,71,72');

-- ----------------------------
-- Table structure for sd_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `sd_auth_group_access`;
CREATE TABLE `sd_auth_group_access`  (
  `uid` mediumint(8) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  UNIQUE INDEX `uid_group_id`(`uid`, `group_id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sd_auth_group_access
-- ----------------------------
INSERT INTO `sd_auth_group_access` VALUES (1, 1);

-- ----------------------------
-- Table structure for sd_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `sd_auth_rule`;
CREATE TABLE `sd_auth_rule`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `pid` smallint(5) UNSIGNED NOT NULL COMMENT '父级ID',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '图标',
  `sort` tinyint(4) UNSIGNED NOT NULL COMMENT '排序',
  `condition` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sd_auth_rule
-- ----------------------------
INSERT INTO `sd_auth_rule` VALUES (1, 'admin/System/default', '系统配置', 1, 1, 0, 'layui-icon-set', 1, '');
INSERT INTO `sd_auth_rule` VALUES (2, 'admin/System/siteConfig', '站点配置', 1, 1, 1, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (3, 'admin/System/updateSiteConfig', '更新配置', 1, 0, 1, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (5, 'admin/Menu/default', '菜单管理', 1, 1, 0, 'layui-icon-menu-fill', 2, '');
INSERT INTO `sd_auth_rule` VALUES (6, 'admin/Menu/index', '后台菜单', 1, 1, 5, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (7, 'admin/Menu/add', '添加菜单', 1, 0, 6, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (8, 'admin/Menu/save', '保存菜单', 1, 0, 6, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (9, 'admin/Menu/edit', '编辑菜单', 1, 0, 6, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (10, 'admin/Menu/update', '更新菜单', 1, 0, 6, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (11, 'admin/Menu/delete', '删除菜单', 1, 0, 6, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (12, 'admin/Nav/index', '导航管理', 1, 0, 5, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (13, 'admin/Category/index', '分类管理', 1, 1, 14, 'fa fa-sitemap', 0, '');
INSERT INTO `sd_auth_rule` VALUES (14, 'admin/Content/default', '内容管理', 1, 1, 0, 'layui-icon-list', 3, '');
INSERT INTO `sd_auth_rule` VALUES (15, 'admin/Vod/index', '视频管理', 1, 1, 14, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (16, 'admin/User/default', '用户管理', 1, 1, 0, 'layui-icon-user', 5, '');
INSERT INTO `sd_auth_rule` VALUES (17, 'admin/User/index', '普通用户', 1, 1, 16, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (18, 'admin/AdminUser/index', '管理员', 1, 1, 16, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (19, 'admin/AuthGroup/index', '权限组', 1, 1, 16, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (20, 'admin/Category/add', '添加分类', 1, 0, 13, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (21, 'admin/Category/save', '保存分类', 1, 0, 13, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (22, 'admin/Category/edit', '编辑分类', 1, 0, 13, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (23, 'admin/Category/update', '更新分类', 1, 0, 13, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (24, 'admin/Category/delete', '删除分类', 1, 0, 13, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (25, 'admin/Vod/add', '添加视频', 1, 0, 15, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (26, 'admin/Vod/save', '保存视频', 1, 0, 15, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (27, 'admin/Vod/edit', '编辑视频', 1, 0, 15, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (28, 'admin/Vod/update', '更新视频', 1, 0, 15, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (29, 'admin/Vod/delete', '删除视频', 1, 0, 15, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (30, 'admin/Vod/toggle', '视频审核', 1, 0, 15, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (31, 'admin/AuthGroup/add', '添加权限组', 1, 0, 19, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (32, 'admin/AuthGroup/save', '保存权限组', 1, 0, 19, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (33, 'admin/AuthGroup/edit', '编辑权限组', 1, 0, 19, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (34, 'admin/AuthGroup/update', '更新权限组', 1, 0, 19, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (35, 'admin/AuthGroup/delete', '删除权限组', 1, 0, 19, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (36, 'admin/AuthGroup/auth', '授权', 1, 0, 19, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (37, 'admin/AuthGroup/updateAuthGroupRule', '更新权限组规则', 1, 0, 19, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (39, 'admin/Nav/add', '添加导航', 1, 0, 12, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (40, 'admin/Nav/save', '保存导航', 1, 0, 12, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (41, 'admin/Nav/edit', '编辑导航', 1, 0, 12, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (42, 'admin/Nav/update', '更新导航', 1, 0, 12, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (43, 'admin/Nav/delete', '删除导航', 1, 0, 12, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (44, 'admin/User/add', '添加用户', 1, 0, 17, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (45, 'admin/User/save', '保存用户', 1, 0, 17, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (46, 'admin/User/edit', '编辑用户', 1, 0, 17, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (47, 'admin/User/update', '更新用户', 1, 0, 17, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (48, 'admin/User/delete', '删除用户', 1, 0, 17, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (49, 'admin/AdminUser/add', '添加管理员', 1, 0, 18, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (50, 'admin/AdminUser/save', '保存管理员', 1, 0, 18, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (51, 'admin/AdminUser/edit', '编辑管理员', 1, 0, 18, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (52, 'admin/AdminUser/update', '更新管理员', 1, 0, 18, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (53, 'admin/AdminUser/delete', '删除管理员', 1, 0, 18, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (54, 'admin/Slide/default', '扩展管理', 1, 1, 0, 'layui-icon-util', 6, '');
INSERT INTO `sd_auth_rule` VALUES (56, 'admin/slide/index', '轮播图', 1, 0, 54, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (57, 'admin/Link/index', '友情链接', 1, 0, 54, 'fa fa-link', 0, '');
INSERT INTO `sd_auth_rule` VALUES (63, 'admin/Slide/add', '添加轮播', 1, 0, 56, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (64, 'admin/Slide/save', '保存轮播', 1, 0, 56, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (65, 'admin/Slide/edit', '编辑轮播', 1, 0, 56, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (66, 'admin/Slide/update', '更新轮播', 1, 0, 56, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (67, 'admin/Slide/delete', '删除轮播', 1, 0, 56, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (68, 'admin/Link/add', '添加链接', 1, 0, 57, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (69, 'admin/Link/save', '保存链接', 1, 0, 57, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (70, 'admin/Link/edit', '编辑链接', 1, 0, 57, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (71, 'admin/Link/update', '更新链接', 1, 0, 57, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (72, 'admin/Link/delete', '删除链接', 1, 0, 57, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (73, 'admin/ChangePassword/index', '修改密码', 1, 1, 1, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (74, 'admin/ChangePassword/updatePassword', '更新密码', 1, 0, 1, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (90, 'admin/help/default', '帮助管理', 1, 1, 0, 'layui-icon-help', 7, '');
INSERT INTO `sd_auth_rule` VALUES (91, 'admin/help_category/index', '分类管理', 1, 1, 90, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (92, 'admin/help/index', '内容管理', 1, 1, 90, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (96, 'admin/Live/default', '电视管理', 1, 1, 0, 'layui-icon-play', 4, '');
INSERT INTO `sd_auth_rule` VALUES (97, 'admin/Live/index', '频道管理', 1, 1, 96, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (99, 'admin/LiveArea/index', '地区管理', 1, 1, 96, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (100, 'admin/Collect/index', '采集管理', 1, 1, 14, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (101, 'admin/update/index', '更新管理', 1, 1, 54, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (102, 'admin/feedback/index', '反馈管理', 1, 1, 14, '', 0, '');
INSERT INTO `sd_auth_rule` VALUES (103, 'admin/database/replace', '批量替换', '1', '1', '54', '', '0', '');
INSERT INTO `sd_auth_rule` VALUES (104, 'admin/Girls/index', '美图管理', '1', '1', '14', '', '0', '');
INSERT INTO `sd_auth_rule` VALUES (105, 'admin/ad_log/index', '广告日志', '1', '1', '14', '', '0', '');

-- ----------------------------
-- Table structure for sd_category
-- ----------------------------
DROP TABLE IF EXISTS `sd_category`;
CREATE TABLE `sd_category`  (
  `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sort` smallint(6) UNSIGNED NOT NULL DEFAULT 0,
  `mid` smallint(6) UNSIGNED NOT NULL DEFAULT 1,
  `pid` smallint(6) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `is_home` tinyint(255) NULL DEFAULT 0,
  `union` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `extend` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type_sort`(`sort`) USING BTREE,
  INDEX `type_pid`(`pid`) USING BTREE,
  INDEX `type_mid`(`mid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'APP分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sd_category
-- ----------------------------
INSERT INTO `sd_category` VALUES (1, '电影', 4, 1, 0, 1, 1, '', '{\"class\":\"\\u559c\\u5267,\\u52a8\\u4f5c,\\u7231\\u60c5,\\u60ca\\u609a,\\u72af\\u7f6a,\\u5192\\u9669,\\u79d1\\u5e7b,\\u60ac\\u7591,\\u5267\\u60c5,\\u52a8\\u753b,\\u6b66\\u4fa0,\\u6218\\u4e89,\\u6b4c\\u821e,\\u5947\\u5e7b,\\u4f20\\u8bb0,\\u8b66\\u532a,\\u5386\\u53f2,\\u8fd0\\u52a8,\\u4f26\\u7406,\\u707e\\u96be,\\u897f\\u90e8,\\u9b54\\u5e7b,\\u67aa\\u6218,\\u6050\\u6016,\\u8bb0\\u5f55\",\"area\":\"\\u5927\\u9646,\\u7f8e\\u56fd,\\u9999\\u6e2f,\\u97e9\\u56fd,\\u82f1\\u56fd,\\u53f0\\u6e7e,\\u65e5\\u672c,\\u6cd5\\u56fd,\\u610f\\u5927\\u5229,\\u5fb7\\u56fd,\\u897f\\u73ed\\u7259,\\u6cf0\\u56fd,\\u5176\\u5b83\",\"lang\":\"\\u56fd\\u8bed,\\u82f1\\u8bed,\\u7ca4\\u8bed,\\u95fd\\u5357\\u8bed,\\u97e9\\u8bed,\\u65e5\\u8bed,\\u6cd5\\u8bed,\\u5fb7\\u8bed,\\u5176\\u5b83\",\"year\":\"2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2009,2008,2006,2005,2004\"}', 0);
INSERT INTO `sd_category` VALUES (2, '电视剧', 3, 1, 0, 1, 1, '', '{\"class\":\"\\u53e4\\u88c5,\\u559c\\u5267,\\u72af\\u7f6a,\\u6050\\u6016,\\u60ca\\u609a,\\u7231\\u60c5,\\u5267\\u60c5,\\u5947\\u5e7b\",\"area\":\"\\u5927\\u9646,\\u97e9\\u56fd,\\u9999\\u6e2f,\\u53f0\\u6e7e,\\u65e5\\u672c,\\u7f8e\\u56fd,\\u6cf0\\u56fd,\\u82f1\\u56fd,\\u65b0\\u52a0\\u5761,\\u5176\\u4ed6,\\u9999\\u6e2f\\u5730\\u533a\",\"lang\":\"\\u56fd\\u8bed,\\u82f1\\u8bed,\\u7ca4\\u8bed,\\u95fd\\u5357\\u8bed,\\u97e9\\u8bed,\\u65e5\\u8bed,\\u5176\\u5b83\",\"year\":\"2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2009,2008,2006,2005,2004\"}', 0);
INSERT INTO `sd_category` VALUES (3, '综艺', 1, 1, 0, 1, 1, '', '{\"class\":\"\\u5927\\u9646\\u7efc\\u827a,\\u97e9\\u56fd\\u7efc\\u827a,\\u9999\\u6e2f\\u7efc\\u827a,\\u53f0\\u6e7e\\u7efc\\u827a,\\u5176\\u5b83\",\"area\":\"\\u5927\\u9646,\\u97e9\\u56fd,\\u9999\\u6e2f,\\u53f0\\u6e7e,\\u7f8e\\u56fd,\\u5176\\u5b83\",\"lang\":\"\\u56fd\\u8bed,\\u82f1\\u8bed,\\u7ca4\\u8bed,\\u95fd\\u5357\\u8bed,\\u97e9\\u8bed,\\u65e5\\u8bed,\\u5176\\u5b83\",\"year\":\"2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004\"}', 0);
INSERT INTO `sd_category` VALUES (4, '动画片', 3, 1, 0, 1, 1, '', '{\"class\":\"\\u56fd\\u4ea7\\u52a8\\u6f2b,\\u52a8\\u6f2b\\u7535\\u5f71,\\u6b27\\u7f8e\\u52a8\\u6f2b,\\u65e5\\u672c\\u52a8\\u6f2b,\\u5267\\u60c5\",\"area\":\"\\u5927\\u9646,\\u65e5\\u672c,\\u6b27\\u7f8e,\\u5176\\u4ed6\",\"lang\":\"\\u56fd\\u8bed,\\u82f1\\u8bed,\\u7ca4\\u8bed,\\u95fd\\u5357\\u8bed,\\u97e9\\u8bed,\\u65e5\\u8bed,\\u5176\\u5b83\",\"year\":\"2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004\",\"star\":\"\",\"director\":\"\",\"state\":\"\",\"version\":\"TV\\u7248,\\u7535\\u5f71\\u7248,OVA\\u7248,\\u771f\\u4eba\\u7248\"}', 0);

-- ----------------------------
-- Table structure for sd_collect
-- ----------------------------
DROP TABLE IF EXISTS `sd_collect`;
CREATE TABLE `sd_collect`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort` int(11) UNSIGNED NULL DEFAULT 0,
  `collect_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `collect_tag` varchar(255)  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '采集标识',
  `collect_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `collect_type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `collect_tv_filter` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `collect_mid` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `collect_appid` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `collect_appkey` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `collect_param` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `collect_opt` int(2) NOT NULL,
  `collect_filter` int(2) NOT NULL DEFAULT 0,
  `collect_filter_from` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `qrcode_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `is_p2p` int(1) DEFAULT '0' COMMENT 'p2p加速',
  `jiexi_urls` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采集接口' ROW_FORMAT = Dynamic;



DROP TABLE IF EXISTS `sd_girls`;
CREATE TABLE `sd_girls` (
                            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                            `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
                            `title` varchar(255) NOT NULL DEFAULT '' COMMENT '链接名称',
                            `url` varchar(255) DEFAULT '' COMMENT '链接地址',
                            `content` text COMMENT '描述',
                            `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态 1 显示  2 隐藏',
                            `create_time` int(11) DEFAULT NULL COMMENT '时间戳',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='美图';




-- ----------------------------
-- Table structure for sd_feedback
-- ----------------------------
DROP TABLE IF EXISTS `sd_feedback`;
CREATE TABLE `sd_feedback`  (
                                `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '文章ID',
                                `sort` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
                                `vid` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '视频ID',
                                `uuid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '设备id',
                                `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
                                `md5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '内容MD5',
                                `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态 0 待审核  1 审核',
                                `create_time` int(11) UNSIGNED NOT NULL COMMENT '创建时间',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章表' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sd_help
-- ----------------------------
DROP TABLE IF EXISTS `sd_help`;
CREATE TABLE `sd_help`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '视频ID',
  `sort` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `affiche` int(1) UNSIGNED NULL DEFAULT NULL,
  `cid` smallint(5) UNSIGNED NOT NULL COMMENT '分类ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `is_home` smallint(5) UNSIGNED NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态 0 待审核  1 审核',
  `create_time` int(11) UNSIGNED NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '视频表' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sd_help_category
-- ----------------------------
DROP TABLE IF EXISTS `sd_help_category`;
CREATE TABLE `sd_help_category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名称',
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '导航别名',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `is_home` smallint(5) UNSIGNED NULL DEFAULT 0,
  `status` smallint(5) NULL DEFAULT 1 COMMENT '状态 1 显示  2 隐藏',
  `create_time` int(11) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sd_help_category
-- ----------------------------
INSERT INTO `sd_help_category` VALUES (45, '常见问题', 'cjwt', 0, 1, 1, 1613531838);
INSERT INTO `sd_help_category` VALUES (46, '网站声明', 'wzsm', 0, 1, 1, 1613531987);
INSERT INTO `sd_help_category` VALUES (47, '协议条款', 'xytk', 0, NULL, 1, 1613531998);
INSERT INTO `sd_help_category` VALUES (48, '站点相关', 'zdxg', 0, NULL, 1, 1613532015);
INSERT INTO `sd_help_category` VALUES (49, '网站公告', 'wzgg', 0, 0, 1, 1614311412);

-- ----------------------------
-- Table structure for sd_live
-- ----------------------------
DROP TABLE IF EXISTS `sd_live`;
CREATE TABLE `sd_live`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '视频ID',
  `cid` int(5) NULL DEFAULT NULL COMMENT '地区ID',
  `sort` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态 0 待审核  1 审核',
  `create_time` int(11) UNSIGNED NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `title`(`title`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '视频表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sd_live_area
-- ----------------------------
DROP TABLE IF EXISTS `sd_live_area`;
CREATE TABLE `sd_live_area`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '视频ID',
  `sort` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态 0 待审核  1 审核',
  `create_time` int(11) UNSIGNED NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `title`(`title`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '视频表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sd_live_area
-- ----------------------------
INSERT INTO `sd_live_area` VALUES (15, 0, '江苏', 1, 1621820101);
INSERT INTO `sd_live_area` VALUES (16, 0, '福建', 1, 1621820110);
INSERT INTO `sd_live_area` VALUES (17, 0, '河北', 1, 1621820128);
INSERT INTO `sd_live_area` VALUES (18, 0, '湖北', 1, 1621820134);
INSERT INTO `sd_live_area` VALUES (19, 0, '重庆', 1, 1621820143);
INSERT INTO `sd_live_area` VALUES (20, 0, '河南', 1, 1621820149);
INSERT INTO `sd_live_area` VALUES (21, 0, '广东', 1, 1621820158);
INSERT INTO `sd_live_area` VALUES (22, 0, '央视', 1, 1621821253);

-- ----------------------------
-- Table structure for sd_message
-- ----------------------------
DROP TABLE IF EXISTS `sd_message`;
CREATE TABLE `sd_message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` int(11) NULL DEFAULT 0 COMMENT '来自谁',
  `to` int(11) NULL DEFAULT 0 COMMENT '接收者',
  `pan_id` int(11) NULL DEFAULT 0 COMMENT '资源id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '消息内容',
  `create_time` int(11) NULL DEFAULT 0 COMMENT '消息时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sd_update
-- ----------------------------
DROP TABLE IF EXISTS `sd_update`;
CREATE TABLE `sd_update`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '链接地址',
  `package_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '包名',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `ver_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ver_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `app_type` varchar(50) DEFAULT '' COMMENT '版本类型',
  `md5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件md5',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态 1 显示  2 隐藏',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '友情链接表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sd_user
-- ----------------------------
DROP TABLE IF EXISTS `sd_user`;
CREATE TABLE `sd_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `realname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '真实姓名',
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '注册邮箱',
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '淘宝唯一标识符',
  `is_real` int(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否实名',
  `vip_id` int(11) NULL DEFAULT 0 COMMENT 'vip 套餐id',
  `vip_time` int(11) NULL DEFAULT 0 COMMENT '会员到期时间',
  `gender` tinyint(1) NULL DEFAULT 0 COMMENT '0 未知 1 男 2女',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '用户状态  1 正常  2 禁止',
  `idcard` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '身份证',
  `login_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登陆类型',
  `create_time` int(10) NULL DEFAULT 0 COMMENT '创建时间',
  `last_login_time` int(10) NOT NULL DEFAULT 0 COMMENT '最后登陆时间',
  `last_login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10089 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `sd_ad_log`;
CREATE TABLE `sd_ad_log` (
                             `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章ID',
                             `uuid` varchar(255) NOT NULL DEFAULT '' COMMENT '图标',
                             `log_type` varchar(255) NOT NULL COMMENT '内容',
                             `status` int(1) unsigned DEFAULT '0',
                             `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='日志表';


-- ----------------------------
-- Table structure for sd_vod
-- ----------------------------
DROP TABLE IF EXISTS `sd_vod`;
CREATE TABLE `sd_vod`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort` int(11) UNSIGNED NULL DEFAULT 0,
  `type_id` smallint(6) NOT NULL DEFAULT 0,
  `type_id_1` smallint(6) UNSIGNED NOT NULL DEFAULT 0,
  `vod_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '视频标题',
  `vod_sub` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `vod_first_letter` varchar(30) NOT NULL DEFAULT '' COMMENT '标题拼音首字母',
  `vod_letter` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_color` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_tag` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_class` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主图URL',
  `vod_pic_thumb` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_pic_slide` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_actor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_director` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_writer` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_behind` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_blurb` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_remarks` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_pubdate` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_total` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `vod_serial` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `vod_tv` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_weekday` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_area` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_lang` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_year` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_version` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_state` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_author` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_isend` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `vod_lock` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `vod_states` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 1,
  `vod_level` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `vod_levels` int(11) NOT NULL DEFAULT 0,
  `vod_copyright` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `vod_hits` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `vod_hits_day` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `vod_hits_week` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `vod_hits_month` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `vod_duration` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_up` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `vod_down` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `vod_score` decimal(3, 1) UNSIGNED NOT NULL DEFAULT 0.0,
  `vod_score_all` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `vod_score_num` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `vod_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `vod_skip_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `vod_time_add` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `vod_time_hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `vod_time_make` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `vod_trysee` smallint(6) UNSIGNED NOT NULL DEFAULT 0,
  `vod_douban_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `vod_douban_score` decimal(3, 1) UNSIGNED NOT NULL DEFAULT 0.0,
  `vod_reurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_rel_vod` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_rel_art` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_custom_sort` int(2) unsigned DEFAULT '0' COMMENT '自定义排序',
  `vod_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vod_play_from` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_play_note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `vod_play_url` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type_id`(`type_id`) USING BTREE,
  INDEX `type_id_1`(`type_id_1`) USING BTREE,
  INDEX `vod_level`(`vod_level`) USING BTREE,
  INDEX `vod_hits`(`vod_hits`) USING BTREE,
  INDEX `vod_letter`(`vod_letter`) USING BTREE,
  INDEX `vod_name`(`vod_name`) USING BTREE,
  INDEX `vod_year`(`vod_year`) USING BTREE,
  INDEX `vod_area`(`vod_area`) USING BTREE,
  INDEX `vod_lang`(`vod_lang`) USING BTREE,
  INDEX `vod_tag`(`vod_tag`) USING BTREE,
  INDEX `vod_class`(`vod_class`) USING BTREE,
  INDEX `vod_lock`(`vod_lock`) USING BTREE,
  INDEX `vod_up`(`vod_up`) USING BTREE,
  INDEX `vod_down`(`vod_down`) USING BTREE,
  INDEX `vod_en`(`vod_en`) USING BTREE,
  INDEX `vod_hits_day`(`vod_hits_day`) USING BTREE,
  INDEX `vod_hits_week`(`vod_hits_week`) USING BTREE,
  INDEX `vod_hits_month`(`vod_hits_month`) USING BTREE,
  INDEX `vod_time_add`(`vod_time_add`) USING BTREE,
  INDEX `vod_time`(`vod_time`) USING BTREE,
  INDEX `vod_skip_time`(`vod_skip_time`) USING BTREE,
  INDEX `vod_time_make`(`vod_time_make`) USING BTREE,
  INDEX `vod_actor`(`vod_actor`) USING BTREE,
  INDEX `vod_director`(`vod_director`) USING BTREE,
  INDEX `vod_score_all`(`vod_score_all`) USING BTREE,
  INDEX `vod_score_num`(`vod_score_num`) USING BTREE,
  INDEX `vod_total`(`vod_total`) USING BTREE,
  INDEX `vod_score`(`vod_score`) USING BTREE,
  INDEX `vod_version`(`vod_version`) USING BTREE,
  INDEX `vod_state`(`vod_state`) USING BTREE,
  INDEX `vod_isend`(`vod_isend`) USING BTREE,
  INDEX `vod_first_letter` (`vod_first_letter`) USING BTREE,
  INDEX `vod_custom_sort`(`vod_custom_sort`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10000 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


SET FOREIGN_KEY_CHECKS = 1;